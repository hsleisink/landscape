<?php
	class list_controller extends Banshee\controller {
		private $list_types = array("security", "files", "input", "internet", "protocols");

		private function show_list() {
			if (in_array($_SESSION["listtype"], $this->list_types) == false) {
				$_SESSION["listtype"] = $this->list_types[0];
			}

			$function = "get_".$_SESSION["listtype"];
			if (method_exists($this->model, $function) == false) {
				$this->view->add_tag("result", "Internal error.");
				return;
			}

			if (($values = $this->model->$function()) === false) {
				$this->view->add_tag("result", $this->view->global_text("error_database"));
				return;
			}

			$this->view->open_tag("list");

			$this->view->open_tag("types", array("selected" => $_SESSION["listtype"]));
			foreach ($this->list_types as $type) {
				$this->view->add_tag("option", LIST_TYPES[$type] ?? "?", array("type" => $type));
			}
			$this->view->close_tag();

			$this->view->open_tag($_SESSION["listtype"]);
			foreach ($values as $value) {
				$this->view->record($value, "record");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = "Lists";
			$this->view->add_css("includes/print.css");

			if (isset($_SESSION["listtype"]) == false) {
				$_SESSION["listtype"] = $this->list_types[0];
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (in_array($_POST["type"], $this->list_types)) {
					$_SESSION["listtype"] = $_POST["type"];
				}
			} else if ($this->page->parameter_value(0, $this->list_types)) {
				$_SESSION["listtype"] = $this->page->parameters[0];
			}

			$this->show_list();
		}
	}
?>
