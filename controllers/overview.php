<?php
	class overview_controller extends Banshee\controller {
		public function execute() {
			$this->view->title = "Overview";

			if (($information = $this->model->get_information()) === false) {
				return false;
			}

			if (($applications = $this->model->get_applications()) === false) {
				return false;
			}

			if (($business = $this->model->get_business()) === false) {
				return false;
			}

			$this->view->add_css("includes/print.css");

			$this->view->add_javascript("overview.js");
			$this->view->add_help_button();

			$this->view->open_tag("overview");

			/* Information
			 */
			$this->view->open_tag("information");
			foreach ($information as $info) {
				$this->view->record($info, "information");
			}
			$this->view->close_tag();

			/* Applications
			 */
			$this->view->open_tag("applications");
			foreach ($applications as $application) {
				$this->view->record($application, "application");
			}
			$this->view->close_tag();

			/* Business
			 */
			$this->view->open_tag("business");
			foreach ($business as $item) {
				$this->view->record($item, "item");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}
	}
?>
