<?php
	class cms_processing_controller extends Banshee\controller {
		private function show_overview() {
            if (($_SESSION["processings_search"] ?? "") == "") {
				if (($processing_count = $this->model->count_processings()) === false) {
					$this->view->add_tag("result", "Database error.");
					return;
				}

				$paging = new Banshee\pagination($this->view, "processings", $this->settings->admin_page_size, $processing_count);

				if (($processings = $this->model->get_processings($paging->offset, $paging->size)) === false) {
					$this->view->add_tag("result", "Database error.");
					return;
				}
            } else {
                if (($processings = $this->model->get_processings()) === false) {
                    $this->view->add_tag("result", "Database error.");
                    return;
                }
            }

			$this->view->open_tag("overview", array("search" => $_SESSION["processing_search"] ?? ""));

			$this->view->open_tag("processings");
			foreach ($processings as $processing) {
				$processing["role"] = PROCESSING_ROLES[$processing["role"]];
				$this->view->record($processing, "processing");
			}
			$this->view->close_tag();

			$paging->show_browse_links();

			$this->view->close_tag();
		}

		private function show_processing_form($processing) {
			if (($information = $this->model->get_information()) === false) {
				$this->view->add_tag("result", "Database error.");
				return false;
			}

			$this->view->add_javascript("cms/processing.js");

			$this->view->open_tag("edit");

			$this->view->record($processing, "processing");

			$this->view->open_tag("information");
			foreach ($information as $info) {
				$this->view->add_tag("information", $info["name"], array("id" => $info["id"]));
			}
			$this->view->close_tag();

			$this->view->open_tag("roles");
			foreach (PROCESSING_ROLES as $role) {
				$this->view->add_tag("role", $role);
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save processing") {
					/* Save processing
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_processing_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create processing
						 */
						if ($this->model->create_processing($_POST) === false) {
							$this->view->add_message("Error creating processing.");
							$this->show_processing_form($_POST);
						} else {
							$this->user->log_action("processing created");
							$this->show_overview();
						}
					} else {
						/* Update processing
						 */
						if ($this->model->update_processing($_POST) === false) {
							$this->view->add_message("Error updating processing.");
							$this->show_processing_form($_POST);
						} else {
							$this->user->log_action("processing updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete processing") {
					/* Delete processing
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_processing_form($_POST);
					} else if ($this->model->delete_processing($_POST["id"]) === false) {
						$this->view->add_message("Error deleting processing.");
						$this->show_processing_form($_POST);
					} else {
						$this->user->log_action("processing deleted");
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "search") {
					/* Search
					 */
					$_SESSION["processing_search"] = $_POST["search"];
					$this->show_overview();
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) == "new") {
				/* New processing
				 */
				$processing = array();
				$this->show_processing_form($processing);
			} else if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit processing
				 */
				if (($processing = $this->model->get_processing($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", "Processing not found.");
				} else {
					$this->show_processing_form($processing);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
