<?php
	class cms_application_link_controller extends Banshee\controller {
		/* Show overview
		 */
		private function show_overview() {
			if (($applications = $this->model->get_applications()) === false) {
				$this->view->add_tag("result", "Error retrieving application list.");
				return;
			} else if (count($applications) == 0) {
				$this->view->add_tag("result", "Add some applications first.", array("url" => "cms/application"));
				return;
			}

			if (isset($_SESSION["application_id"]) == false) {
				$_SESSION["application_id"] = (int)$applications[0]["id"];
			}

			if (($application = $this->model->get_application($_SESSION["application_id"])) == false) {
				unset($_SESSION["application_id"]);
				$this->view->add_tag("result", "Previously selected application not found.");
				return;
			}

			if (($allocation = $this->model->get_allocation_list($_SESSION["application_id"])) === false) {
				$this->view->add_tag("result", "Error while retrieving allocation.");
				return;
			}

			if (($connections = $this->model->get_connection_list($_SESSION["application_id"])) === false) {
				$this->view->add_tag("result", "Error while retrieving connections.");
				return;
			}

			if (($file_operations = $this->model->get_file_operation_list($_SESSION["application_id"])) === false) {
				$this->view->add_tag("result", "Error while retrieving file operations.");
				return;
			}

			if (($used_by = $this->model->get_usedby_list($_SESSION["application_id"])) === false) {
				$this->view->add_tag("result", "Error while retrieving application usage.");
				return;
			}

			if (($runs_at = $this->model->get_runsat_list($_SESSION["application_id"])) === false) {
				$this->view->add_tag("result", "Error while retrieving used hardware.");
				return;
			}

			$this->view->add_help_button();

			$this->view->open_tag("overview");

			$this->view->open_tag("applications", array("selected" => $_SESSION["application_id"]));
			foreach ($applications as $application) {
				$this->view->record($application, "application");
			}
			$this->view->close_tag();

			$this->view->open_tag("allocation");
			foreach ($allocation as $info) {
				$info["type"] = INFORMATION_STORAGE_TYPES[$info["type"]];
				$this->view->record($info, "information");
			}
			$this->view->close_tag();

			$this->view->open_tag("connections");
			foreach ($connections as $connection) {
				$connection["data_flow"] = CONNECTION_DATA_FLOW[$connection["data_flow"]];
				$this->view->record($connection, "connection");
			}
			$this->view->close_tag();

			$this->view->open_tag("file_operations");
			foreach ($file_operations as $file_operation) {
				$file_operation["data_flow"] = FILE_DATA_FLOW[$file_operation["data_flow"]];
				$this->view->record($file_operation, "file_operation");
			}
			$this->view->close_tag();

			$this->view->open_tag("used_by");
			foreach ($used_by as $entity) {
				$this->view->record($entity, "entity");
			}
			$this->view->close_tag();

			$this->view->open_tag("runs_at");
			foreach ($runs_at as $device) {
				$this->view->record($device, "device");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_application($application_id) {
			if (($application = $this->model->get_application($application_id)) == false) {
				return;
			}

			$this->view->add_tag("application", $application["name"]);
		}

		/* Show information allocation form
		 */
		private function show_allocation_form($allocation) {
			if (($information = $this->model->get_information()) === false) {
				$this->view->add_tag("result", "Error retrieving application list.");
				return;
			}

			$this->view->add_help_button();

			$attr = isset($allocation["id"]) ? array("id" => $allocation["id"]) : array();
			$this->view->open_tag("allocation", $attr);

			$this->view->record($allocation);
			$this->show_application($allocation["application_id"]);

			$this->view->open_tag("information");
			foreach ($information as $info) {
				$this->view->record($info, "information");
			}
			$this->view->close_tag();

			$this->view->open_tag("storage");
			foreach (INFORMATION_STORAGE_TYPES as $i => $type) {
				$this->view->add_tag("type", $type, array("id" => $i));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		/* Show connection form
		 */
		private function show_connection_form($connection) {
			if (($applications = $this->model->get_applications()) == false) {
				$this->view->add_tag("result", "Error retrieving application list.");
				return;
			}

			$this->view->add_help_button();

			$attr = isset($connection["id"]) ? array("id" => $connection["id"]) : array();
			$this->view->open_tag("connection", $attr);

			$this->view->record($connection);
			$this->show_application($connection["from_application_id"]);

			$this->view->open_tag("applications");
			foreach ($applications as $application) {
				$this->view->record($application, "application");
			}
			$this->view->close_tag();

			$this->view->open_tag("data_flow");
			foreach (CONNECTION_DATA_FLOW as $i => $direction) {
				$this->view->add_tag("direction", $direction, array("id" => $i));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		/* Show file operation form
		 */
		private function show_file_operation_form($file_operation) {
			$attr = isset($file_operation["id"]) ? array("id" => $file_operation["id"]) : array();
			$this->view->open_tag("file_operation", $attr);

			$this->view->record($file_operation);
			$this->show_application($file_operation["application_id"]);

			$this->view->open_tag("data_flow");
			foreach (FILE_DATA_FLOW as $i => $direction) {
				$this->view->add_tag("direction", $direction, array("id" => $i));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		/* Show used-by form
		 */
		private function show_usedby_form($usedby) {
			if (($business = $this->model->get_business()) == false) {
				$this->view->add_tag("result", "No business entities present. Add some first.");
				return;
			}

			$this->view->add_help_button();

			$attr = isset($usedby["id"]) ? array("id" => $usedby["id"]) : array();
			$this->view->open_tag("usedby", $attr);

			$this->view->record($usedby);
			$this->show_application($usedby["application_id"]);

			$this->view->open_tag("business");
			foreach ($business as $entity) {
				$this->view->record($entity, "entity");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		/* Show runs-at form
		 */
		private function show_runsat_form($runsat) {
			if (($hardware = $this->model->get_hardware()) == false) {
				$this->view->add_tag("result", "No hardware present. Add some first.");
				return;
			}

			$attr = isset($runsat["id"]) ? array("id" => $runsat["id"]) : array();
			$this->view->open_tag("runsat", $attr);

			$this->view->record($runsat);
			$this->show_application($runsat["application_id"]);

			$this->view->open_tag("hardware");
			foreach ($hardware as $device) {
				$this->view->record($device, "device");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		/* Execute
		 */
		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "set application") {
					if ($this->model->valid_application_id($_POST["application_id"])) {
						$_SESSION["application_id"] = (int)$_POST["application_id"];
					}
					$this->show_overview();
				} else if ($_POST["submit_button"] == "Save allocation") {
					/* Save allocation
					 */
					if ($this->model->allocation_oke($_POST) == false) {
						$this->show_allocation_form($_POST);
					} else if ($this->model->save_allocation($_POST) == false) {
						$this->view->add_message("Error saving allocation.");
						$this->show_allocation_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Delete allocation") {
					/* Delete allocation
					 */
					if ($this->model->delete_allocation($_POST["id"]) == false) {
						$this->view->add_message("Error deleting allocation.");
						$this->show_allocation_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Save connection") {
					/* Save connection
					 */
					if ($this->model->connection_oke($_POST) == false) {
						$this->show_connection_form($_POST);
					} else if ($this->model->save_connection($_POST) == false) {
						$this->view->add_message("Error saving connection.");
						$this->show_connection_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Delete connection") {
					/* Delete connection
					 */
					if ($this->model->delete_connection($_POST["id"]) == false) {
						$this->view->add_message("Error deleting connection.");
						$this->show_connection_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Save file operation") {
					/* Save file operation
					 */
					if ($this->model->file_operation_oke($_POST) == false) {
						$this->show_file_operation_form($_POST);
					} else if ($this->model->save_file_operation($_POST) == false) {
						$this->view->add_message("Error saving file operation.");
						$this->show_file_operation_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Delete file operation") {
					/* Delete file operation
					 */
					if ($this->model->delete_file_operation($_POST["id"]) == false) {
						$this->view->add_message("Error deleting file operation.");
						$this->show_file_operation_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Save used-by") {
					/* Save used-by
					 */
					if ($this->model->usedby_oke($_POST) == false) {
						$this->show_usedby_form($_POST);
					} else if ($this->model->save_usedby($_POST) == false) {
						$this->view->add_message("Error saving used-by.");
						$this->show_usedby_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Delete used-by") {
					/* Delete used-by
					 */
					if ($this->model->delete_usedby($_POST["id"]) == false) {
						$this->view->add_message("Error deleting used-by.");
						$this->show_usedby_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Save runs-at") {
					/* Save runs-at
					 */
					if ($this->model->runsat_oke($_POST) == false) {
						$this->show_runsat_form($_POST);
					} else if ($this->model->save_runsat($_POST) == false) {
						$this->view->add_message("Error saving runs-at.");
						$this->show_runsat_form($_POST);
					} else {
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Delete runs-at") {
					/* Delete runs-at
					 */
					if ($this->model->delete_runsat($_POST["id"]) == false) {
						$this->view->add_message("Error deleting runs-at.");
						$this->show_runsat_form($_POST);
					} else {
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) == "information") {
				/* Allocation
				 */
				if (valid_input($this->page->parameters[1] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
					$this->show_allocation_form(array("application_id" => $_SESSION["application_id"]));
				} else if (($allocation = $this->model->get_allocation($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", "Allocation not found.");
				} else {
					$this->show_allocation_form($allocation);
				}
			} else if (($this->page->parameters[0] ?? null) == "connection") {
				/* Connection
				 */
				if (valid_input($this->page->parameters[1] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
					$this->show_connection_form(array("from_application_id" => $_SESSION["application_id"]));
				} else if (($connection = $this->model->get_connection($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", "Connection not found.");
				} else {
					$this->show_connection_form($connection);
				}
			} else if (($this->page->parameters[0] ?? null) == "fileoperation") {
				/* File operation
				 */
				if (valid_input($this->page->parameters[1] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
					$this->show_file_operation_form(array("application_id" => $_SESSION["application_id"]));
				} else if (($file_operation = $this->model->get_file_operation($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", "File operation not found.");
				} else {
					$this->show_file_operation_form($file_operation);
				}
			} else if (($this->page->parameters[0] ?? null) == "usedby") {
				/* Used by
				 */
				if (valid_input($this->page->parameters[1] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
					$this->show_usedby_form(array("application_id" => $_SESSION["application_id"]));
				} else if (($usedby = $this->model->get_usedby($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", "Used-by not found.");
				} else {
					$this->show_usedby_form($usedby);
				}
			} else if (($this->page->parameters[0] ?? null) == "runsat") {
				/* Runs at
				 */
				if (valid_input($this->page->parameters[1] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
					$this->show_runsat_form(array("application_id" => $_SESSION["application_id"]));
				} else if (($runsat = $this->model->get_runsat($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", "Runs-at not found.");
				} else {
					$this->show_runsat_form($runsat);
				}
			} else {
				if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
					if ($this->model->valid_application_id($this->page->parameters[0])) {
						$_SESSION["application_id"] = (int)$this->page->parameters[0];
					}
				}

				$this->show_overview();
			}
		}
	}
?>
