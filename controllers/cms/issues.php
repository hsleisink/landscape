<?php
	class cms_issues_controller extends Banshee\controller {
		private function show_list($list, $list_label, $item_label) {
			$this->view->open_tag($list_label);
			foreach ($list as $item) {
				$this->view->record($item, $item_label);
			}
			$this->view->close_tag();
		}

		private function show_issues() {
			if (($info_without_owner = $this->model->get_info_without_owner()) === false) {
				return false;
			}

			if (($storage_issues = $this->model->get_storage_issues()) === false) {
				return false;
			}

			if (($apps_without_owner = $this->model->get_apps_without_owner()) === false) {
				return false;
			}

			if (($apps_without_hardware = $this->model->get_apps_without_hardware()) === false) {
				return false;
			}

			if (($crowded = $this->model->get_crowded_servers()) === false) {
				return false;
			}

			if (($isolated_business = $this->model->get_isolated_business()) === false) {
				return false;
			}

			if (($isolated_hardware = $this->model->get_isolated_hardware()) === false) {
				return false;
			}

			if (($privacy_issues = $this->model->get_privacy_issues()) === false) {
				return false;
			}

			$this->view->open_tag("overview");
			$this->show_list($info_without_owner, "info_without_owner", "information");
			$this->show_list($storage_issues, "storage_issues", "information");
			$this->show_list($apps_without_owner, "apps_without_owner", "application");
			$this->show_list($apps_without_hardware, "apps_without_hardware", "application");
			$this->show_list($crowded, "crowded", "device");
			$this->show_list($isolated_business, "isolated_business", "business");
			$this->show_list($isolated_hardware, "isolated_hardware", "device");
			$this->show_list($privacy_issues, "privacy_issues", "processing");
			$this->view->close_tag();

			return true;
		}

		public function execute() {
			if ($this->show_issues() == false) {
				$this->view->add_tag("result", "Database error.");
			}
		}
	}
?>
