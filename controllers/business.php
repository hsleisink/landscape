<?php
	class business_controller extends Banshee\controller {
		private $url = array("url" => "overview");

		private function show_overview() {
			if (($business = $this->model->get_business_list()) === false) {
				$this->view->add_tag("result", $this->view->global_text("error_database"), $this->url);
				return false;
			}

			$this->view->open_tag("overview");
			foreach ($business as $entity) {
				$this->view->record($entity, "entity");
			}
			$this->view->close_tag();
		}

		private function show_business($business_id) {
			if (($business = $this->model->get_business($business_id)) === false) {
				$this->view->add_tag("result", "Business entity not found.", $this->url);
				return false;
			}

			if (($application_ownership = $this->model->get_application_ownership($business_id)) === false) {
				$this->view->add_tag("result", "Error getting application ownership.", $this->url);
				return false;
			}

			if (($application_usage = $this->model->get_application_usage($business_id)) === false) {
				$this->view->add_tag("result", "Error getting application usage.", $this->url);
				return false;
			}

			if (($information = $this->model->get_information($business_id)) === false) {
				$this->view->add_tag("result", "Error getting information.", $this->url);
				return false;
			}

			$this->view->add_javascript('banshee/jquery.windowframe.js');
			$this->view->add_javascript("dialog.js");

			$this->view->title = $business["name"];

			$this->view->open_tag("business", array("id" => $business_id, "previous" => $this->page->previous));

			$this->view->record($business);

			$this->view->open_tag("labels");
			foreach ($business["labels"] as $label) {
				$this->view->add_tag("label", $label["name"], array(
					"id" => $label["id"],
					"cid" => $label["category_id"]));
			}
			$this->view->close_tag();

			$this->view->open_tag("application_usage");
			foreach ($application_usage as $item) {
				$this->view->record($item, "item");
			}
			$this->view->close_tag();

			$this->view->open_tag("application_ownership");
			foreach ($application_ownership as $item) {
				$this->view->record($item, "item");
			}
			$this->view->close_tag();

			$this->view->open_tag("information");
			foreach ($information as $info) {
				$this->view->record($info, "information");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = $this->language->global_text("business");
			$this->view->add_css("includes/print.css");

			if ($this->page->parameter_numeric(0)) {
				$this->show_business((int)$this->page->parameters[0]);
			} else {
				$this->show_overview();
			}
		}
	}
?>
