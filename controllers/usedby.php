<?php
	class usedby_controller extends Banshee\controller {
		private $url = array("url" => "overview");

		private function show_usedby($id) {
			if (($usedby = $this->model->get_usedby($id)) == false) {
				$this->view->add_tag("result", "Used-by not found.", $this->url);
				return;
			}

			$this->view->record($usedby, "usedby", array("previous" => $this->page->previous));
		}

		public function execute() {
			if ($this->page->parameter_numeric(0)) {
				$this->show_usedby($this->page->parameters[0]);
			} else {
				$this->view->add_tag("result", "No used-by specified.", $this->url);
			}
		}
	}
?>
