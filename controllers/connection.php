<?php
	class connection_controller extends Banshee\controller {
		private $url = array("url" => "overview");

		private function show_connection($id) {
			if (($connection = $this->model->get_connection($id)) == false) {
				$this->view->add_tag("result", "Connection not found.", $this->url);
				return;
			}

			$connection["data_flow"] = CONNECTION_DATA_FLOW[$connection["data_flow"]];
			$this->view->record($connection, "connection", array("previous" => $this->page->previous));
		}

		public function execute() {
			if ($this->page->parameter_numeric(0)) {
				$this->show_connection($this->page->parameters[0]);
			} else {
				$this->view->add_tag("result", "No connection specified.", $this->url);
			}
		}
	}
?>
