<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-striped table-condensed overview">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/module/type" /></th>
<th><xsl:value-of select="/output/language/global/owner" /></th>
<th><xsl:value-of select="/output/language/global/location" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="application">
<tr>
<td><a href="/{/output/page}/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="type" /></td>
<td><xsl:value-of select="owner" /></td>
<td><xsl:value-of select="location" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/overview" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Application template
//
//-->
<xsl:template match="application">
<xsl:if test="type!='' or description!=''">
<div class="panel panel-body panel-default">
<xsl:if test="type!=''"><div><xsl:value-of select="/output/language/module/type" />: <xsl:value-of select="type" /></div></xsl:if>
<xsl:if test="description!=''"><div><xsl:value-of select="description" /></div></xsl:if>
</div>
</xsl:if>


<div class="row">
<div class="col-sm-4"><xsl:value-of select="/output/language/global/owner" />: <xsl:if test="owner!=''"><a href="/business/{owner_id}"><xsl:value-of select="owner" /></a></xsl:if></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/global/location" />: <xsl:value-of select="location" /></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/module/internet_facing" />: <xsl:value-of select="internet" /></div>
</div>

<div class="row">
<div class="col-sm-4"><xsl:value-of select="/output/language/global/confidentiality" />: <xsl:value-of select="confidentiality" /></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/global/integrity" />: <xsl:value-of select="integrity" /></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/global/availability" />: <xsl:value-of select="availability" /></div>
</div>

<xsl:if test="labels/label">
<div class="labels"><xsl:value-of select="/output/language/global/labels" />:<xsl:for-each select="labels/label">
<xsl:if test="position()>1"><xsl:if test="@cid!=preceding::*[1]/@cid"><span>,</span></xsl:if></xsl:if>
<a href="/label/{@id}" class="label label-primary"><xsl:value-of select="." /></a>
</xsl:for-each></div>
</xsl:if>

<h2><xsl:value-of select="/output/language/global/information" /></h2>
<table class="table table-striped table-condensed overview">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/global/owner" /></th>
<th><xsl:value-of select="/output/language/global/storage_type" /></th>
<th><xsl:value-of select="/output/language/global/security_need" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="information/information">
<tr>
<td><a href="/information/{@id}"><xsl:value-of select="name" /></a></td>
<td><a href="/business/{owner_id}"><xsl:value-of select="owner" /></a></td>
<td><xsl:value-of select="type" /></td>
<td><xsl:value-of select="value" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2><xsl:value-of select="/output/language/module/connections" /></h2>
<table class="table table-striped table-condensed table-xs connections">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/global/from" /></th>
<th><xsl:value-of select="/output/language/global/to" /></th>
<th><xsl:value-of select="/output/language/global/protocol" /></th>
<th><xsl:value-of select="/output/language/global/format" /></th>
<th><xsl:value-of select="/output/language/global/frequency" /></th>
<th><xsl:value-of select="/output/language/global/data_flow" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="connections/connection">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/from" /></span><a href="/application/{from_application_id}"><xsl:value-of select="from_name" /></a></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/to" /></span><a href="/application/{to_application_id}"><xsl:value-of select="to_name" /></a></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/protocol" /></span><xsl:value-of select="protocol" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/format" /></span><xsl:value-of select="format" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/freqency" /></span><xsl:value-of select="frequency" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/data_flow" /></span><xsl:value-of select="data_flow" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/information" /></span><xsl:if test="description!=''"><span class="glyphicon glyphicon-info-sign" onClick="javascript:show_dialog('con', {@id});" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<xsl:if test="file_operations/file_operation">
<h2><xsl:value-of select="/output/language/global/file_operations" /></h2>
<table class="table table-striped table-condensed table-xs file-operations">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/global/location" /></th>
<th><xsl:value-of select="/output/language/global/format" /></th>
<th><xsl:value-of select="/output/language/global/frequency" /></th>
<th><xsl:value-of select="/output/language/global/data_flow" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="file_operations/file_operation">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/location" /></span><xsl:value-of select="location" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/format" /></span><xsl:value-of select="format" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/frequency" /></span><xsl:value-of select="frequency" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/data_flow" /></span><xsl:value-of select="data_flow" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/information" /></span><xsl:if test="description!=''"><span class="glyphicon glyphicon-info-sign" onClick="javascript:show_dialog('file', {@id});" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<div class="row">
<div class="col-sm-6">

<h2><xsl:value-of select="/output/language/global/used_by" /></h2>
<table class="table table-striped table-condensed usedby">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/global/information_input" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="used_by/entity">
<tr>
<td><a href="/business/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="input" /></td>
<td><xsl:if test="description!=''"><span class="glyphicon glyphicon-info-sign" onClick="javascript:show_dialog('use', {@id});" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

</div>
<div class="col-sm-6">

<h2><xsl:value-of select="/output/language/module/runs_at" /></h2>
<table class="table table-striped table-condensed runsat">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/global/operating_system" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="runs_at/device">
<tr>
<td><a href="/hardware/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="os" /></td>
<td><xsl:if test="description!=''"><span class="glyphicon glyphicon-info-sign" onClick="javascript:show_dialog('run', {@id});" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

</div>
</div>

<xsl:if test="processings/processing">
<h2>Processing activities</h2>
<table class="table table-striped table-condensed table-hover processing">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/description" /></th>
<th><xsl:value-of select="/output/language/global/role" /></th>
<th>Usage</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="processings/processing">
<tr onClick="javascript:document.location='/processing/{@id}'">
<td><xsl:value-of select="description" /></td>
<td><xsl:value-of select="role" /></td>
<td><xsl:value-of select="usage" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<div class="btn-group">
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
<a href="/application" class="btn btn-default"><xsl:value-of select="/output/language/module/all_applications" /></a>
</div>

<div class="dialogs">
<xsl:for-each select="connections/connection">
<xsl:if test="description!=''">
<div id="des_con_{@id}" title="{from_name} &#8594; {to_name}"><xsl:value-of select="description" /></div>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="file_operations/file_operation">
<xsl:if test="description!=''">
<div id="des_file_{@id}" title="File at {location}"><xsl:value-of select="description" /></div>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="used_by/entity">
<xsl:if test="description!=''">
<div id="des_use_{@id}" title="Used by {name}"><xsl:value-of select="description" /></div>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="runs_at/device">
<xsl:if test="description!=''">
<div id="des_run_{@id}" title="Runs at {name}"><xsl:value-of select="description" /></div>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="collections/collection">
<xsl:if test="description!=''">
<div id="des_dat_{@id}" title="Data collection {name}"><xsl:value-of select="description" /></div>
</xsl:if>
</xsl:for-each>
</div>

<div id="help">
<ul>
<xsl:value-of select="/output/language/module/help_location" disable-output-escaping="yes" />
<xsl:value-of select="/output/language/global/help_classification" disable-output-escaping="yes" />
</ul>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/application.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="application" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
