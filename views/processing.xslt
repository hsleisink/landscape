<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-striped table-condensed table-hover overview">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/information" /></th>
<th><xsl:value-of select="/output/language/global/description" /></th>
<th><xsl:value-of select="/output/language/global/role" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="processings/processing">
<tr onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="description" /></td>
<td><xsl:value-of select="role" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="?output=csv" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_export_to_csv" /></a>
</div>
</xsl:template>

<!--
//
//  Processing template
//
//-->
<xsl:template match="processing">
<p><xsl:value-of select="/output/language/global/information" />: <a href="/information/{info_id}"><xsl:value-of select="name" /></a>&#160;<xsl:value-of select="/output/language/module/as" />&#160;<xsl:value-of select="role" />.</p>
<p><xsl:value-of select="description" /></p>

<h2><xsl:value-of select="/output/language/module/processing_applications" /></h2>
<table class="table table-condensed table-striped table-xs">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/application" /></th>
<th><xsl:value-of select="/output/language/global/owner" /></th>
<th><xsl:value-of select="/output/language/global/storage_type" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="applications/application">
<tr>
<td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td>
<td><a href="/business/{owner_id}"><xsl:value-of select="owner" /></a></td>
<td><xsl:value-of select="type" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="panel panel-default">
<xsl:if test="role='controller'">
<div class="panel-heading"><xsl:value-of select="/output/language/module/contact_controller" /></div>
</xsl:if>
<xsl:if test="role='processor'">
<div class="panel-heading"><xsl:value-of select="/output/language/module/contact_processor" /></div>
</xsl:if>
<div class="panel-body"><xsl:value-of select="contact" /></div>
</div>

<div class="panel panel-default">
<xsl:if test="role='controller'">
<div class="panel-heading"><xsl:value-of select="/output/language/module/processing_purpose" /></div>
</xsl:if>
<xsl:if test="role='processor'">
<div class="panel-heading"><xsl:value-of select="/output/language/module/processing_categories" /></div>
</xsl:if>
<div class="panel-body"><xsl:value-of select="purpose" /></div>
</div>

<xsl:if test="role='controller'">
<div class="panel panel-default">
<div class="panel-heading"><xsl:value-of select="/output/language/module/data_subjects" /></div>
<div class="panel-body"><xsl:value-of select="subject" /></div>
</div>

<div class="panel panel-default">
<div class="panel-heading"><xsl:value-of select="/output/language/module/recipients" /></div>
<div class="panel-body"><xsl:value-of select="recipient" /></div>
</div>
</xsl:if>

<div class="panel panel-default">
<div class="panel-heading"><xsl:value-of select="/output/language/module/data_transfer" /></div>
<div class="panel-body"><xsl:value-of select="transfer" /></div>
</div>

<xsl:if test="role='controller'">
<div class="panel panel-default">
<div class="panel-heading"><xsl:value-of select="/output/language/module/data_erasure" /></div>
<div class="panel-body"><xsl:value-of select="erasure" /></div>
</div>
</xsl:if>

<div class="panel panel-default">
<div class="panel-heading"><xsl:value-of select="/output/language/module/security_measures" /></div>
<div class="panel-body"><xsl:value-of select="security" /></div>
</div>

<div class="btn-group">
<xsl:if test="/output/page!=@previous">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_view_all" /></a>
</xsl:if>
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/processing.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="processing" />
<xsl:apply-templates select="result" />

<div id="help">
<xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" />
</div>
</xsl:template>

</xsl:stylesheet>
