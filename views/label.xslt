<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<div class="categories row">
<xsl:for-each select="category">
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
<h2><xsl:value-of select="@name" /></h2>
<div class="list-group">
<xsl:for-each select="label">
<a href="/{/output/page}/{@id}" class="list-group-item"><xsl:value-of select="." /><span class="badge"><xsl:value-of select="@count" /></span></a>
</xsl:for-each>
</div>
</div>
</xsl:for-each>
</div>

<div id="help">
<xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Label template
//
//-->
<xsl:template match="label">
<div class="row">
<div class="col-sm-4">

<h2><xsl:value-of select="/output/language/global/information" /></h2>
<table class="table table-striped label-condensed">
<thead>
<tr><th><xsl:value-of select="/output/language/module/name" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="information/information">
<tr><td><a href="/information/{@id}"><xsl:value-of select="name" /></a></td></tr>
</xsl:for-each>
</tbody>
</table>

</div>
<div class="col-sm-4">

<h2><xsl:value-of select="/output/language/global/applications" /></h2>
<table class="table table-striped label-condensed">
<thead>
<tr><th><xsl:value-of select="/output/language/module/name" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="applications/application">
<tr><td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td></tr>
</xsl:for-each>
</tbody>
</table>

</div>
<div class="col-sm-4">

<h2><xsl:value-of select="/output/language/global/business" /></h2>
<table class="table table-striped label-condensed">
<thead>
<tr><th><xsl:value-of select="/output/language/module/name" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="business/entity">
<tr><td><a href="/business/{@id}"><xsl:value-of select="name" /></a></td></tr>
</xsl:for-each>
</tbody>
</table>

</div>
</div>

<div class="btn-group">
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/labels.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="label" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
