<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-striped table-condensed overview">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/module/description" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="entity">
<tr>
<td><a href="/{/output/page}/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="description" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/overview" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Business template
//
//-->
<xsl:template match="business">
<xsl:if test="description!=''"><div class="panel panel-default panel-body"><xsl:value-of select="description" /></div></xsl:if>

<div><xsl:value-of select="/output/language/global/labels" />:<xsl:for-each select="labels/label">
<xsl:if test="position()>1"><xsl:if test="@cid!=preceding::*[1]/@cid"><span>,</span></xsl:if></xsl:if>
<a href="/label/{@id}" class="label label-primary"><xsl:value-of select="." /></a>
</xsl:for-each></div>

<xsl:if test="count(application_usage/item)>0">
<h2><xsl:value-of select="/output/language/module/application_usage" /></h2>
<table class="table table-striped table-condensed app-use">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/global/information_input" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="application_usage/item">
<tr>
<td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="input" /></td>
<td><xsl:if test="description!=''"><span class="glyphicon glyphicon-info-sign" onClick="javascript:show_dialog('appuse', {@id});" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<xsl:if test="count(application_ownership/item)>0">
<h2><xsl:value-of select="/output/language/module/application_ownership" /></h2>
<table class="table table-striped table-condensed app-own">
<thead>
<tr><th><xsl:value-of select="/output/language/module/owner" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="application_ownership/item">
<tr><td><a href="/application/{@id}"><xsl:value-of select="name" /></a></td></tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>

<h2><xsl:value-of select="/output/language/global/information" /></h2>
<table class="table table-striped table-condensed overview">
<thead>
<tr><th><xsl:value-of select="/output/language/global/name" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="information/information">
<tr>
<td><a href="/information/{@id}"><xsl:value-of select="name" /></a></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
<a href="/business" class="btn btn-default"><xsl:value-of select="/output/language/module/all_business" /></a>
</div>

<div class="dialogs">
<xsl:for-each select="application_usage/item">
<xsl:if test="description!=''">
<div id="des_appuse_{@id}" title="Used by {name}"><span><xsl:value-of select="description" /></span></div>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="data_collection_usage/item">
<xsl:if test="description!=''">
<div id="des_datuse_{@id}" title="Used by {name}"><span><xsl:value-of select="description" /></span></div>
</xsl:if>
</xsl:for-each>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/business.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="business" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
