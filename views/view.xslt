<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover views">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/name" /></th>
<th><xsl:value-of select="/output/language/module/locked" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="views/view">
<tr class="click" onClick="javascript:document.location='/{/output/page}/build/{@id}'">
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="locked" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_view" /></a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<div class="row">
<div class="col-xs-12 col-sm-6">
<form action="/{/output/page}" method="post">
<xsl:if test="view/@id">
<input type="hidden" name="id" value="{view/@id}" />
</xsl:if>

<label for="name"><xsl:value-of select="/output/language/global/name" />:</label>
<input type="text" id="name" name="name" value="{view/name}" class="form-control" />
<label for="width"><xsl:value-of select="/output/language/module/width" />:</label>
<input type="text" id="width" name="width" value="{view/width}" class="form-control" />
<label for="height"><xsl:value-of select="/output/language/module/height" />:</label>
<input type="text" id="height" name="height" value="{view/height}" class="form-control" />
<xsl:if test="view/@id">
<label for="locked"><xsl:value-of select="/output/language/module/locked" />:</label>
<input type="checkbox" id="locked" name="locked"><xsl:if test="view/locked='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_view}" class="btn btn-default" />
<a href="/{/output/page}/build/{view/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="view/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_view}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</div>
<div class="col-xs-12 col-sm-6">
<table class="table table-striped table-hover sizes">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/size" /></th>
<th><xsl:value-of select="/output/language/module/width" /></th>
<th><xsl:value-of select="/output/language/module/height" /></th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div>
</xsl:template>

<!--
//
//  Build template
//
//-->
<xsl:template match="build">
<div class="btn-group btn-top">
<xsl:if test="view/locked='no'">
<button class="btn btn-default" onClick="javascript:frame_add()"><xsl:value-of select="/output/language/module/btn_add_frame" /></button>
</xsl:if>
<button class="btn btn-default" onClick="javascript:window.print()"><xsl:value-of select="/output/language/module/btn_print" /></button>
<button class="btn btn-default help">Help</button>
<a href="/view/{view/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_settings" /></a>
<a href="/view" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>

<xsl:if test="view/locked='no'">
<div class="sidebar">
<ul class="list-group">
<li class="list-group-item active"><xsl:value-of select="/output/language/global/applications" /><span class="btn btn-xs btn-primary" onClick="javascript:list_toggle('a')">-</span></li>
<xsl:for-each select="application">
<li class="list-group-item a"><input type="checkbox" toggles="a{@id}" onChange="javascript:element_toggle('a{@id}')"><xsl:if test="x!=''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="name" /></li>
</xsl:for-each>
<li class="list-group-item active"><xsl:value-of select="/output/language/global/business" /><span class="btn btn-xs btn-primary" onClick="javascript:list_toggle('b')">-</span></li>
<xsl:for-each select="business">
<li class="list-group-item b"><input type="checkbox" toggles="b{@id}" onChange="javascript:element_toggle('b{@id}')"><xsl:if test="x!=''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="name" /></li>
</xsl:for-each>
<li class="list-group-item active"><xsl:value-of select="/output/language/global/information" /><span class="btn btn-xs btn-primary" onClick="javascript:list_toggle('i')">-</span></li>
<xsl:for-each select="information">
<li class="list-group-item i"><input type="checkbox" toggles="i{@id}" onChange="javascript:element_toggle('i{@id}')"><xsl:if test="x!=''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="name" /></li>
</xsl:for-each>
<li class="list-group-item active"><xsl:value-of select="/output/language/global/file_operations" /><span class="btn btn-xs btn-primary" onClick="javascript:list_toggle('f')">-</span></li>
<xsl:for-each select="file_operation">
<li class="list-group-item f"><input type="checkbox" toggles="f{@id}" onChange="javascript:element_toggle('f{@id}')"><xsl:if test="x!=''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="location" /></li>
</xsl:for-each>
<li class="list-group-item active"><xsl:value-of select="/output/language/global/hardware" /><span class="btn btn-xs btn-primary" onClick="javascript:list_toggle('h')">-</span></li>
<xsl:for-each select="hardware">
<li class="list-group-item h"><input type="checkbox" toggles="h{@id}" onChange="javascript:element_toggle('h{@id}')"><xsl:if test="x!=''"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="name" /></li>
</xsl:for-each>
</ul>
</div>
</xsl:if>

<div class="view-container">
<div class="view" view_id="{view/@id}" style="width:{view/width}px; height:{view/height}px;" locked="{view/locked}">
<xsl:for-each select="frame">
<div frame_id="{@id}" class="frame" style="left:{@x}px; top:{@y}px; width:{@width}px; height:{@height}px;"><div class="frame-header"><span class="frame-title"><xsl:value-of select="." /></span><span class="frame-delete">&#215;</span></div></div>
</xsl:for-each>
<xsl:for-each select="application">
<div id="a{@id}" class="element application" style="left:{x}px; top:{y}px;"><xsl:if test="x=''"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if><span><xsl:value-of select="name" /></span><span><a href="/application/{@id}"><xsl:value-of select="/output/language/module/view" /></a></span></div>
</xsl:for-each>
<xsl:for-each select="business">
<div id="b{@id}" class="element business" style="left:{x}px; top:{y}px;"><xsl:if test="x=''"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if><span><xsl:value-of select="name" /></span><span><a href="/business/{@id}"><xsl:value-of select="/output/language/module/view" /></a></span></div>
</xsl:for-each>
<xsl:for-each select="information">
<div id="i{@id}" class="element information" style="left:{x}px; top:{y}px;"><xsl:if test="x=''"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if><span><xsl:value-of select="name" /></span><span><a href="/information/{@id}"><xsl:value-of select="/output/language/module/view" /></a></span></div>
</xsl:for-each>
<xsl:for-each select="file_operation">
<div id="f{@id}" class="element file_operation" style="left:{x}px; top:{y}px;"><xsl:if test="x=''"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if><span><xsl:value-of select="location" /></span></div>
</xsl:for-each>
<xsl:for-each select="hardware">
<div id="h{@id}" class="element hardware" style="left:{x}px; top:{y}px;"><xsl:if test="x=''"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if><span><xsl:value-of select="name" /></span><span><a href="/hardware/{@id}"><xsl:value-of select="/output/language/module/view" /></a></span></div>
</xsl:for-each>
</div>
</div>

<div class="links">
<xsl:for-each select="link">
<div class="link" type="{@type}" from="{@from}" to="{@to}"><xsl:if test="@type='connection' or @type='operation'"><xsl:attribute name="flow"><xsl:value-of select="@flow" /></xsl:attribute></xsl:if><xsl:value-of select="." /></div>
</xsl:for-each>
</div>

<div class="help">
<xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/views.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="build" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
