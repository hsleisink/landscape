<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  File operation template
//
//-->
<xsl:template match="file_operation">
<dl>
	<dt><xsl:value-of select="/output/language/global/application" />:</dt>
	<dd><a href="/application/{application_id}"><xsl:value-of select="application" /></a></dd>
	<dt><xsl:value-of select="/output/language/global/location" />:</dt>
	<dd><xsl:value-of select="location" /></dd>
	<dt><xsl:value-of select="/output/language/global/format" />:</dt>
	<dd><xsl:value-of select="format" /></dd>
	<dt><xsl:value-of select="/output/language/global/frequency" />:</dt>
	<dd><xsl:value-of select="frequency" /></dd>
	<dt><xsl:value-of select="/output/language/global/data_flow" />:</dt>
	<dd><xsl:value-of select="data_flow" /></dd>
	<dt><xsl:value-of select="/output/language/global/description" />:</dt>
	<dd class="description"><xsl:value-of select="description" /></dd>
</dl>

<div class="btn-group">
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language//btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/file_operation" /></h1>
<xsl:apply-templates select="file_operation" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
