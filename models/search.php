<?php
	class search_model extends Banshee\model {
		private $text = null;
		private $add_or = null;

		public function __call($name, $args) {
			return false;
		}

		/* Truncate text
		 */
		function truncate_text($text, $length) {
			if (strlen($text) <= $length) {
				return $text;
			}

			$is_space = ($text[$length] === " ");
			$text = substr($text, 0, $length);
			if ($is_space == false) {
				if (($pos = strrpos($text, " ")) !== false) {
					$text = substr($text, 0, $pos);
				}
			}

			return $text."...";
		}

		/* Add selection to query
		 */
		private function add_selection($column, &$query, &$args) {
			if ($this->add_or) {
				$query .= " or ";
			}
			$query .= "(".$column. " like %s)";
			array_push($args, "%".$this->text."%");

			$this->add_or = true;
		}

		/* Add boolean to query
		 */
		private function add_boolean($value, $column, &$query, &$args) {
			if (strtolower($this->text) != $value) {
				return;
			}

			if ($this->add_or) {
				$query .= " or ";
			}

			$query .= "(".$column."=%d)";
			array_push($args, YES);

			$this->add_or = true;
		}

		/* Add enum to query
		 */
		private function add_enum($enum, $column, &$query, &$args) {
			if (in_array($this->text, $enum) == false) {
				return;
			}

			$flipped = array_flip($enum);
			$value = $flipped[$this->text];

			if ($this->add_or) {
				$query .= " or ";
			}

			$query .= "(".$column."=%s)";
			array_push($args, $value);

			$this->add_or = true;
		}

		/* Search applications
		 */
		private function search_applications() {
			$query = "select concat(%s, id) as url, name as text, description as content ".
			         "from applications where organisation_id=%d and (";
			$args = array("/application/", $this->user->organisation_id);
			$this->add_selection("name", $query, $args);
			$this->add_selection("type", $query, $args);
			$this->add_selection("description", $query, $args);
			$this->add_enum(CONFIDENTIALITY, "confidentiality", $query, $args);
			$this->add_enum(INTEGRITY, "integrity", $query, $args);
			$this->add_enum(AVAILABILITY, "availability", $query, $args);
			$this->add_enum(LOCATION, "location", $query, $args);
			$query .= ") order by name desc";

			return $this->db->execute($query, $args);
		}

		/* Search business
		 */
		private function search_business() {
			$query = "select concat(%s, id) as url, name as text, description as content from ".
			         "business where organisation_id=%d and (";
			$args = array("/business/", $this->user->organisation_id);
			$this->add_selection("name", $query, $args);
			$this->add_selection("description", $query, $args);
			$query .= ") order by name desc";

			return $this->db->execute($query, $args);
		}

		/* Search data collections
		 */
		private function search_information() {
			$query = "select concat(%s, id) as url, name as text, description as content from ".
			         "information where organisation_id=%d and (";
			$args = array("/information/", $this->user->organisation_id);
			$this->add_selection("name", $query, $args);
			$this->add_selection("description", $query, $args);
			$query .= ") order by name desc";

			return $this->db->execute($query, $args);
		}

		/* Search hardware
		 */
		private function search_hardware() {
			$query = "select concat(%s, id) as url, name as text, concat(os, %s, description) as content ".
			         "from hardware where organisation_id=%d and (";
			$args = array("/hardware/", ". ", $this->user->organisation_id);
			$this->add_selection("name", $query, $args);
			$this->add_selection("os", $query, $args);
			$this->add_selection("description", $query, $args);
			$query .= ") order by name desc";

			return $this->db->execute($query, $args);
		}

		/* Search connections
		 */
		private function search_connections() {
			$query = "select concat(%s, c.id) as url, c.description as content, ".
			         "(select name from applications where id=c.from_application_id) as from_name, ".
			         "(select name from applications where id=c.to_application_id) as to_name ".
			         "from connections c, applications a where c.from_application_id=a.id and a.organisation_id=%d and (";
			$args = array("/connection/", $this->user->organisation_id);
			$this->add_selection("c.description", $query, $args);
			$this->add_selection("protocol", $query, $args);
			$this->add_selection("format", $query, $args);
			$this->add_selection("frequency", $query, $args);
			$this->add_enum(CONNECTION_DATA_FLOW, "data_flow", $query, $args);
			$query .= ") order by c.description desc";

			if (($result = $this->db->execute($query, $args)) === false) {
				return false;
			}

			foreach ($result as $key => $value) {
				$result[$key]["text"] = $value["from_name"]." ~ ". $value["to_name"];
			}

			return $result;
		}

		/* Search file operations
		 */
		private function search_file_operations() {
			$query = "select concat(%s, f.id) as url, name as text, f.description as content, f.location ".
			         "from file_operations f, applications a where f.application_id=a.id and a.organisation_id=%d and (";
			$args = array("/fileoperation/", $this->user->organisation_id);
			$this->add_selection("f.description", $query, $args);
			$this->add_selection("f.location", $query, $args);
			$this->add_selection("format", $query, $args);
			$this->add_selection("frequency", $query, $args);
			$this->add_enum(FILE_DATA_FLOW, "data_flow", $query, $args);
			$query .= ") order by f.description desc";

			if (($result = $this->db->execute($query, $args)) === false) {
				return false;
			}

			foreach ($result as $key => $value) {
				$result[$key]["text"] = $value["location"]." at ". $value["text"];
			}

			return $result;
		}

		/* Search used-by
		 */
		private function search_usedby() {
			$query = "select concat(%s, u.id) as url, u.description as content, ".
			         "(select name from applications where id=u.application_id) as application, ".
			         "(select name from business where id=u.business_id) as business ".
			         "from application_business u, applications a where u.application_id=a.id and a.organisation_id=%d and (";
			$args = array("/usedby/", $this->user->organisation_id);
			$this->add_selection("u.description", $query, $args);
			$this->add_selection("input", $query, $args);
			$query .= ") order by u.description desc";

			if (($result = $this->db->execute($query, $args)) === false) {
				return false;
			}

			foreach ($result as $key => $value) {
				$result[$key]["text"] = $value["business"]." ~ ". $value["application"];
			}

			return $result;
		}

		/* Search processing
		 */
		public function search_processing() {
			$query = "select concat(%s, p.id) as url, p.description as text, purpose as content ".
			         "from processings p, information i where p.information_id=i.id and i.organisation_id=%d and (";
			$args = array("/processing/", $this->user->organisation_id);
			$this->add_selection("p.description", $query, $args);
			$this->add_enum(PROCESSING_ROLES, "role", $query, $args);
			$this->add_selection("contact", $query, $args);
			$this->add_selection("purpose", $query, $args);
			$this->add_selection("subject", $query, $args);
			$this->add_selection("recipient", $query, $args);
			$this->add_selection("transfer", $query, $args);
			$this->add_selection("erasure", $query, $args);
			$this->add_selection("security", $query, $args);
			$query .= ") order by p.description desc";

			return $this->db->execute($query, $args);
		}

		private function sort($a, $b) {
			$ca = count($a);
			$cb = count($b);

			if ($ca < $cb) {
				return 1;
			} else if ($ca > $cb) {
				return -1;
			}

			return 0;
		}

		/* Search the database
		 */
		public function search($post, $sections) {
			$this->text = $post["query"];
			$result = array();

			foreach ($sections as $section => $label) {
				if (is_true($post[$section])) {
					$this->add_or = false;
					$hits = call_user_func(array($this, "search_".$section));
					if ($hits != false) {
						$result[$section] = $hits;
					}
				}
			}

			uasort($result, array($this, "sort"));

			return $result;
		}
	}
?>
