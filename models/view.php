<?php
	class view_model extends Banshee\model {
		public function get_views() {
			$query = "select * from views where organisation_id=%d order by name";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_view($view_id) {
			$query = "select * from views where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $view_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		/* Core elements
		 */
		public function get_applications($view_id) {
			$query = "select a.id, a.name, a.owner_id, v.x, v.y from applications a ".
			         "left join view_application v on a.id=v.application_id and view_id=%d ".
			         "where a.organisation_id=%d order by name";

			return $this->db->execute($query, $view_id, $this->user->organisation_id);
		}

		public function get_business($view_id) {
			$query = "select b.id, b.name, v.x, v.y from business b ".
			         "left join view_business v on b.id=v.business_id and view_id=%d ".
			         "where b.organisation_id=%d order by name";

			return $this->db->execute($query, $view_id, $this->user->organisation_id);
		}

		public function get_hardware($view_id) {
			$query = "select h.id, h.name, v.x, v.y from hardware h ".
			         "left join view_hardware v on h.id=v.hardware_id and view_id=%d ".
			         "where h.organisation_id=%d order by name";

			return $this->db->execute($query, $view_id, $this->user->organisation_id);
		}

		/* Sub elements
		 */
		public function get_information($view_id) {
			$query = "select i.id, i.owner_id, i.name, v.x, v.y from information i ".
			         "left join view_information v on i.id=v.information_id and view_id=%d ".
					 "where i.organisation_id=%d order by name";

			return $this->db->execute($query, $view_id, $this->user->organisation_id);
		}

		public function get_file_operations($view_id) {
			$query = "select f.id, f.application_id, f.location, f.format, f.data_flow, v.x, v.y ".
			         "from applications a, file_operations f ".
			         "left join view_file_operation v on f.id=v.file_operation_id and view_id=%d ".
					 "where f.application_id=a.id and a.organisation_id=%d order by name";

			return $this->db->execute($query, $view_id, $this->user->organisation_id);
		}

		/* Links
		 */
		public function get_connections() {
			$query = "select c.* from connections c, applications a ".
			         "where c.from_application_id=a.id and a.organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_app_runs_at() {
			$query = "select r.* from application_hardware r, applications a ".
			         "where r.application_id=a.id and a.organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_app_used_by() {
			$query = "select u.* from application_business u, applications a ".
			         "where u.application_id=a.id and a.organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_information_allocation() {
			$query = "select * from information_application l, information i ".
			         "where l.information_id=i.id and i.organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		/* Operations
		 */
		public function save_oke($view) {
			$result = true;

			if (isset($view["id"])) {
				if ($this->get_view($view["id"]) == false) {
					$this->view->add_message($this->language->module_text("error_view_not_found"));
					return false;
				}
			}

			if (trim($view["name"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_name"));
				$result = false;
			}

			if (($view["width"] < 500) || ($view["width"] > 50000)) {
				$this->view->add_message($this->language->module_text("error_width_invalid"));
				$result = false;
			}

			if (($view["height"] < 500) || ($view["height"] > 50000)) {
				$this->view->add_message($this->language->module_text("error_height_invalid"));
				$result = false;
			}

			return $result;
		}

		public function create_view($view) {
			$keys = array("id", "organisation_id", "name", "width", "height", "locked");

			$view["id"] = null;
			$view["organisation_id"] = $this->user->organisation_id;
			$view["locked"] = NO;

			if ($this->db->insert("views", $view, $keys) == false) {
				return false;
			}

			return $this->db->last_insert_id;
		}

		public function update_view($view) {
			$keys = array("name", "width", "height", "locked");

			$view["locked"] = is_true($view["locked"] ?? false) ? YES : NO;

			return $this->db->update("views", $view["id"], $view, $keys);
		}

		public function delete_oke($view) {
			$result = true;

			if ($this->get_view($view["id"]) == false) {
				$this->view->add_message($this->language->module_text("error_view_not_found"));
				return false;
			}

			return $result;
		}

		public function delete_view($view_id) {
			$queries = array(
				array("delete from view_application where view_id=%d", $view_id),
				array("delete from view_business where view_id=%d", $view_id),
				array("delete from view_file_operation where view_id=%d", $view_id),
				array("delete from view_hardware where view_id=%d", $view_id),
				array("delete from view_information where view_id=%d", $view_id),
				array("delete from views where id=%d", $view_id));

			return $this->db->transaction($queries);
		}

		/* Actions
		 */
		private function valid_view_id($view_id) {
			$query = "select count(*) as count from views where id=%d and organisation_id=%d and locked=%d";

			if (($result = $this->db->execute($query, $view_id, $this->user->organisation_id, NO)) == false) {
				return false;
			}

			return (int)$result[0]["count"] > 0;
		}

		private function valid_element_id($type, $element_id) {
			$tables = array(
				"a" => "applications",
				"b" => "business",
				"f" => "file_operations",
				"i" => "information",
				"h" => "hardware");

			if (($table = $tables[$type]) == null) {
				return false;
			}

			if ($type == "f") {
				$query = "select count(*) as count from %S t, applications a ".
				         "where t.application_id=a.id and t.id=%d and a.organisation_id=%d";
			} else {
				$query = "select count(*) as count from %S where id=%d and organisation_id=%d";
			}

			if (($result = $this->db->execute($query, $table, $element_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return (int)$result[0]["count"] > 0;
		}

		private function get_dbs(&$element_id, $view_id) {
            $type = substr($element_id, 0, 1);
            $element_id = (int)substr($element_id, 1);

			$dbs = array(
				"a" => "application",
				"b" => "business",
				"f" => "file_operation",
				"i" => "information",
				"h" => "hardware");

			if (($dbs = $dbs[$type]) == null) {
				return false;
			}

			if ($this->valid_view_id($view_id) == false) {
				return false;
			}

			if ($this->valid_element_id($type, $element_id) == false) {
				return false;
			}

			return $dbs;
		}

		public function show_element($data) {
			if (($dbs = $this->get_dbs($data["element_id"], $data["view_id"])) == false) {
				return false;
			}

			$query = "select count(*) as count from %S where view_id=%d and %S=%d";

			if (($result = $this->db->execute($query, "view_".$dbs, $data["view_id"], $dbs."_id", $data["element_id"])) == false) {
				return false;
			} else if ($result[0]["count"] > 0) {
				return false;
			}

			$link = array(
				"view_id"  => $data["view_id"],
				$dbs."_id" => $data["element_id"],
				"x"        => $data["x"],
				"y"        => $data["y"]);

			return $this->db->insert("view_".$dbs, $link) != false;
		}

		public function hide_element($data) {
			if (($dbs = $this->get_dbs($data["element_id"], $data["view_id"])) == false) {
				return false;
			}

			$query = "delete from %S where view_id=%d and %S=%d";

			return $this->db->query($query, "view_".$dbs, $data["view_id"], $dbs."_id", $data["element_id"]) !== false;
		}

		public function move_element($data) {
			if (($dbs = $this->get_dbs($data["element_id"], $data["view_id"])) == false) {
				return false;
			}

			$query = "update %S set x=%d, y=%d where view_id=%d and %S=%d";

			return $this->db->query($query, "view_".$dbs, $data["x"], $data["y"], $data["view_id"], $dbs."_id", $data["element_id"]) !== false;
		}

		/* Frame functions
		 */
		public function get_frames($view_id) {
			$query = "select f.* from view_frames f, views v ".
			         "where f.view_id=v.id and v.id=%d and v.organisation_id=%d";

			return $this->db->execute($query, $view_id, $this->user->organisation_id);
		}

		public function create_frame($data) {
			if ($this->valid_view_id($data["view_id"]) == false) {
				return false;
			}

			$keys = array("id", "view_id", "title", "x", "y", "width", "height");
			$data["id"] = null;

			if ($this->db->insert("view_frames", $data, $keys) === false) {
				return false;
			}

			return $this->db->last_insert_id;
		}

		public function move_frame($data) {
			if ($this->valid_view_id($data["view_id"]) == false) {
				return false;
			}

			$query = "update view_frames set x=%d, y=%d where id=%d and view_id=%d";
			$this->db->query($query, $data["x"], $data["y"], $data["frame_id"], $data["view_id"]);
		}

		public function resize_frame($data) {
			if ($this->valid_view_id($data["view_id"]) == false) {
				return false;
			}

			$query = "update view_frames set width=%d, height=%d where id=%d and view_id=%d";
			$this->db->query($query, $data["width"], $data["height"], $data["frame_id"], $data["view_id"]);
		}

		public function rename_frame($data) {
			if ($this->valid_view_id($data["view_id"]) == false) {
				return false;
			}

			$query = "update view_frames set title=%s where id=%d and view_id=%d";
			$this->db->query($query, $data["title"], $data["frame_id"], $data["view_id"]);
		}

		public function delete_frame($data) {
			if ($this->valid_view_id($data["view_id"]) == false) {
				return false;
			}

			$query = "delete from view_frames where id=%d and view_id=%d";
			$this->db->query($query, $data["frame_id"], $data["view_id"]);
		}
	}
?>
