<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_data_model extends Banshee\model {
		public function get_organisation($id) {
			if (($organisation = $this->db->entry("organisations", $id)) == false) {
				return false;
			}

			return $organisation["name"];
		}

		private function export_table($table, $column, $value) {
			if (is_array($value) == false) {
				$query = "select * from %S where %S=%s";
			} else if (($count = count($value)) > 0) {
				$filter = implode(",", array_fill(1, $count, "%d"));
				$query = "select * from %S where %S in (".$filter.")";
			} else {
				return array();
			}

			return $this->db->execute($query, $table, $column, $value);
		}

		private function get_id_list($items) {
			$list = array();

			foreach ($items as $item) {
				array_push($list, (int)$item["id"]);
			}

			return $list;
		}

		public function get_export() {
			$export = array("database_version" => $this->settings->database_version);

			if (($export["applications"] = $this->export_table("applications", "organisation_id", $this->user->organisation_id)) === false) {
				return false;
			}

			if (($export["business"] = $this->export_table("business", "organisation_id", $this->user->organisation_id)) === false) {
				return false;
			}

			if (($export["hardware"] = $this->export_table("hardware", "organisation_id", $this->user->organisation_id)) === false) {
				return false;
			}

			if (($export["information"] = $this->export_table("information", "organisation_id", $this->user->organisation_id)) === false) {
				return false;
			}

			if (($export["views"] = $this->export_table("views", "organisation_id", $this->user->organisation_id)) === false) {
				return false;
			}

			/* Labels
			 */
			if (($export["label_categories"] = $this->export_table("label_categories", "organisation_id", $this->user->organisation_id)) === false) {
				return false;
			}

			$category_ids = $this->get_id_list($export["label_categories"]);
			if (($export["labels"] = $this->export_table("labels", "category_id", $category_ids)) === false) {
				return false;
			}

			$label_ids = $this->get_id_list($export["labels"]);
			$tables = array("label_application", "label_business", "label_information");
			foreach ($tables as $table) {
				if (($export[$table] = $this->export_table($table, "label_id", $label_ids)) === false) {
					return false;
				}
			}

			/* Application related links
			 */
			$application_ids = $this->get_id_list($export["applications"]);
			$tables = array("application_hardware", "application_business", "file_operations", "information_application");
			foreach ($tables as $table) {
				if (($export[$table] = $this->export_table($table, "application_id", $application_ids)) === false) {
					return false;
				}
			}

			if (($export["connections"] = $this->export_table("connections", "from_application_id", $application_ids)) === false) {
				return false;
			}

			/* Processings
			 */
			$information_ids = $this->get_id_list($export["information"]);
			if (($export["processings"] = $this->export_table("processings", "information_id", $information_ids)) === false) {
				return false;
			}

			/* Views
			 */
			$view_tables = array("application", "business", "file_operation", "frames", "hardware", "information");
			$view_ids = $this->get_id_list($export["views"]);
			foreach ($view_tables as $table) {
				if (($export["view_".$table] = $this->export_table("view_".$table, "view_id", $view_ids)) === false) {
					return false;
				}
			}

			return $export;
		}

		public function signature($export) {
			$hash = hash("sha256", json_encode($export));

			return hash_hmac("sha256", $hash, $this->settings->secret_website_code);
		}

		public function generate_filename($str) {
			$valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789() ";

			$result = "";
			$len = strlen($str);
			for ($i = 0; $i < $len; $i++) {
				$c = substr($str, $i, 1);
				if (strpos($valid, $c) !== false) {
					$result .= $c;
				}
			}

			return $result;
		}

		private function set_organisation($data, $table) {
			foreach ($data[$table] as $i => $item) {
				$data[$table][$i]["organisation_id"] = $this->user->organisation_id;
			}

			return $data;
		}

		private function import_table($table, $data, $foreign_keys, &$inserted_keys) {
			foreach ($data as $item) {
				$id = null;
				if (isset($item["id"])) {
					$id = (int)$item["id"];
					$item["id"] = null;
				}

				foreach ($foreign_keys as $column => $other_table) {
					if ($item[$column] === null) {
						continue;
					}

					if (($item[$column] = $inserted_keys[$other_table][$item[$column]]) == null) {
						return false;
					}
				}

				if ($this->db->insert($table, $item) === false) {
					return false;
				}

				if ($id !== null) {
					$inserted_keys[$table][$id] = $this->db->last_insert_id;
				}
			}

			return true;
		}

		public function import_data($data) {
			if ($this->delete_data() == false) {
				return false;
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			/* Adjust data
			 */
			$tables = array("information", "applications", "business", "hardware", "label_categories", "views");
			foreach ($tables as $table) {
				$data = $this->set_organisation($data, $table);
			}

			/* Store data
			 */
			$tables = array(
				"business"                => array(),
				"hardware"                => array(),
				"applications"            => array("owner_id" => "business"),
				"application_hardware"    => array("application_id" => "applications", "hardware_id" => "hardware"),
				"application_business"    => array("application_id" => "applications", "business_id" => "business"),
				"connections"             => array("from_application_id" => "applications", "to_application_id" => "applications"),
				"file_operations"         => array("application_id" => "applications"),
				"information"             => array("owner_id" => "business"),
				"information_application" => array("information_id" => "information", "application_id" => "applications"),
				"label_categories"        => array(),
				"labels"                  => array("category_id" => "label_categories"),
				"label_application"       => array("label_id" => "labels", "application_id" => "applications"),
				"label_business"          => array("label_id" => "labels", "business_id" => "business"),
				"label_information"       => array("label_id" => "labels", "information_id" => "information"),
				"processings"             => array("information_id" => "information"),
				"views"                   => array(),
				"view_application"        => array("view_id" => "views", "application_id" => "applications"),
				"view_business"           => array("view_id" => "views", "business_id" => "business"),
				"view_file_operation"     => array("view_id" => "views", "file_operation_id" => "file_operations"),
				"view_frames"             => array("view_id" => "views"),
				"view_hardware"           => array("view_id" => "views", "hardware_id" => "hardware"),
				"view_information"        => array("view_id" => "views", "information_id" => "information"));


			$inserted_keys = array();
			foreach (array_keys($tables) as $table) {
				$inserted_keys[$table] = array();
			}

			foreach ($tables as $table => $foreign_keys) {
				if (is_array($data[$table]) == false) {
					continue;
				}

				if ($this->import_table($table, $data[$table], $foreign_keys, $inserted_keys) == false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($data) {
			return $data["confirmation"] == "Delete all data";
		}

		public function delete_data() {
			$query = "select id from applications where organisation_id=%d";
			if (($applications = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}
			$application_ids = $this->get_id_list($applications);

			$query = "select id from label_categories where organisation_id=%d";
			if (($label_categories = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}
			$label_category_ids = $this->get_id_list($label_categories);

			if ($this->db->query("begin") === false) {
				return false;
			}

			/* Delete data
			 */
			if (($application_count = count($application_ids)) > 0) {
				$tables = array("application", "business", "file_operation", "frames", "hardware", "information");
				foreach ($tables as $table) {
					$query = "delete from %S where view_id in (select id from views where organisation_id=%d)";
					if ($this->db->query($query, "view_".$table, $this->user->organisation_id) === false) {
						return false;
					}
				}

				$filter = implode(",", array_fill(1, $application_count, "%d"));

				$query = "delete from %S where application_id in (".$filter.")";
				$tables = array("application_hardware", "application_business", "file_operations", "information_application");
				foreach ($tables as $table) {
					if ($this->db->query($query, $table, $application_ids) === false) {
						return false;
					}
				}

				$query = "delete from connections where from_application_id in (".$filter.")";
				if ($this->db->query($query, $application_ids) === false) {
					return false;
				}
			}

			$query = "delete from %S where label_id in (select l.id from labels l, label_categories c ".
			         "where l.category_id=c.id and c.organisation_id=%d)";
			$tables = array("label_application", "label_business", "label_information");
			foreach ($tables as $table) {
				if ($this->db->query($query, $table, $this->user->organisation_id) === false) {
					return false;
				}
			}

			$query = "delete from labels where category_id in ".
			         "(select id from label_categories where organisation_id=%d)";
			if ($this->db->query($query, $this->user->organisation_id) === false) {
				return false;
			}

			$query = "delete from processings where information_id in ".
			         "(select id from information where organisation_id=%d)";
			if ($this->db->query($query, $this->user->organisation_id) === false) {
				return false;
			}

			$tables = array("information", "applications", "business", "hardware", "label_categories", "views");
			foreach ($tables as $table) {
				$query = "delete from %S where organisation_id=%d";
				if ($this->db->query($query, $table, $this->user->organisation_id) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
