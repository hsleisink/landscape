<?php
	class cms_processing_model extends Banshee\model {
		private $columns = array("description", "contact", "purpose", "subject", "recipient", "transfer", "erasure", "security");

		public function count_processings() {
			$query = "select count(*) as count from processings p, information i ".
			         "where p.information_id=i.id and i.organisation_id=%d";

			if (($result = $this->db->execute($query, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_processings($offset = null, $limit = null) {
			$query = "select p.description, p.id, p.role, i.name from processings p, information i ".
			         "where p.information_id=i.id and i.organisation_id=%d";
			$args = array($this->user->organisation_id);

			if (($_SESSION["processing_search"] ?? "") != "") {
				foreach ($this->columns as $i => $column) {
					$this->columns[$i] = "p.".$column." like %s";
					array_push($args, "%".$_SESSION["processing_search"]."%");
				}
				$query .= " and (".implode(" or ", $this->columns).")";
			}

			if ($offset !== null) {
				$query .= " limit %d,%d";
				array_push($args, $offset, $limit);
			}

			return $this->db->execute($query, $args);
		}

		public function get_information() {
			$query = "select * from information where organisation_id=%d order by name";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_processing($processing_id) {
			$query = "select p.*, i.name from processings p, information i ".
			         "where p.information_id=i.id and p.id=%d and i.organisation_id=%d";

			if (($result = $this->db->execute($query, $processing_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function save_oke($processing) {
			$result = true;

			if (isset($processing["id"])) {
				if (($current = $this->get_processing($processing["id"])) == false) {
					$this->view->add_message("Processing not found.");
					$this->user->log_action("unauthorized update attempt of processing %d", $processing["id"]);
					return false;
				}
			}

			$query = "select count(*) as count from information where id=%d and organisation_id=%d";
			if (($result = $this->db->execute($query, $processing["information_id"], $this->user->organisation_id)) === false) {
				return false;
			}
			if ($result[0]["count"] == 0) {
				return false;
			}

			if (trim($processing["description"]) == "") {
				$this->view->add_message("Enter the description.");
				$result = false;
			}

			return $result;
		}

		public function create_processing($processing) {
			$keys = array("id", "information_id", "description", "role", "contact", "purpose", "subject", "recipient", "transfer", "erasure", "security");

			$processing["id"] = null;

			return $this->db->insert("processings", $processing, $keys) !== false;
		}

		public function update_processing($processing) {
			$keys = array("information_id", "description", "contact", "purpose", "subject", "recipient", "transfer", "erasure", "security");

			return $this->db->update("processings", $processing["id"], $processing, $keys) !== false;
		}

		public function delete_oke($processing) {
			$result = true;

			if (($current = $this->get_processing($processing["id"])) == false) {
				$this->view->add_message("Processing not found.");
				$this->user->log_action("unauthorized delete attempt of processing %d", $processing["id"]);
				$result = false;
			}

			return $result;
		}

		public function delete_processing($processing_id) {
			return $this->db->delete("processings", $processing_id);
		}
	}
?>
