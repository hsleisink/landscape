<?php
	class cms_information_model extends Banshee\model {
		private $order_columns = array("name", "owner_id");
		private $search_columns = array("i.name", "owner", "i.description");

		public function get_help() {
			$query = "select * from languages where module=%s and name=%s";
			if (($result = $this->db->execute($query, "*", "help_classification")) == false) {
				return false;
			}

			return $result[0]["en"];
		}

		public function count_information() {
			$query = "select count(*) as count from information where organisation_id=%d";

			if (($result = $this->db->execute($query, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_all_information($offset = null, $limit = null) {
			unset($_SESSION["information_order"]);
			if (isset($_SESSION["information_order"]) == false) {
				$_SESSION["information_order"] = array("name", "description");
			}

			if (isset($_GET["order"])) {
				if ((in_array($_GET["order"], $this->order_columns)) && ($_GET["order"] != $_SESSION["information_order"][0])) {
					$_SESSION["information_order"] = array($_GET["order"], $_SESSION["information_order"][0]);
				}
			}

			$query = "select i.*, b.name as owner ".
			         "from information i left join business b on i.owner_id=b.id ".
			         "where i.organisation_id=%d";
			$args = array($this->user->organisation_id);

			$search_columns = array();
			if (($_SESSION["information_search"] ?? "") != "") {
				foreach ($this->search_columns as $i => $column) {
					array_push($search_columns, $column." like %s");
					array_push($args, "%".$_SESSION["information_search"]."%");
				}
				$query .= " having (".implode(" or ", $search_columns).")";
			}

			$query .= " order by i.%S,i.%S";
			array_push($args, $_SESSION["information_order"]);

			if ($offset !== null) {
				$query .= " limit %d,%d";
				array_push($args, $offset, $limit);
			}

			return $this->db->execute($query, $args);
		}

		public function get_information($information_id) {
			$query = "select * from information where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $information_id, $this->user->organisation_id)) == false) {
				return false;
			}
			$information = $result[0];

			$query = "select l.* from label_information l, information i ".
			         "where l.information_id=i.id and information_id=%d and i.organisation_id=%d";
			if (($labels = $this->db->execute($query, $information_id, $this->user->organisation_id)) === false) {
				return false;
			}

			$information["labels"] = array();
			foreach ($labels as $label) {
				array_push($information["labels"], (int)$label["label_id"]);
			}

			return $information;
		}

		public function get_business() {
			$query = "select * from business where organisation_id=%d order by name";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function get_labels() {
			return $this->borrow("label")->get_labels();
		}

		private function get_owner_id($information) {
			if ($information["owner_type"] == "new") {
				if (trim($information["owner_name"]) == "") {
					return null;
				}

				if (($business = $this->db->entry("business", $information["owner_name"], "name")) === false) {
					return false;
				} else if ($business !== null) {
					return (int)$business["id"];
				}

				$owner = array(
					"organisation_id" => $this->user->organisation_id,
					"name"            => $information["owner_name"],
					"description"     => "");
				if ($this->db->insert("business", $owner) === false) {
					return false;
				}

				return $this->db->last_insert_id;
			}

			if ($information["owner_type"] == "existing") {
				if ($information["owner_id"] == 0) {
					return null;
				}

				if (($business = $this->get_business()) === false) {
					return false;
				}

				foreach ($business as $item) {
					if ($item["id"] == $information["owner_id"]) {
						return (int)$information["owner_id"];
					}
				}

				return false;
			}

			return null;
		}

		public function save_oke($information) {
			$result = true;

			if (isset($information["id"])) {
				if ($this->get_information($information["id"]) == false) {
					$this->view->add_message("Information not found.");
					$this->user->log_action("unauthorized update attempt of information %d", $information["id"]);
					return false;
				}
			}

			$information["name"] = trim($information["name"]);

			if ($information["name"] == "") {
				$this->view->add_message("Enter the information name.");
				$result = false;
			} else {
				$query = "select count(*) as count from information where name=%s and organisation_id=%d";
				$args = array($information["name"], $this->user->organisation_id);
				if (isset($information["id"])) {
					$query .= " and id!=%d";
					array_push($args, $information["id"]);
				}

				if (($result = $this->db->execute($query, $args)) === false) {
					$this->view->add_message("Database error.");
					return false;
				}
				if ($result[0]["count"] > 0) {
					$this->view->add_message("This name already exists.");
					$result = false;
				}
			}

			if (($information["owner_type"] == "existing") && ($information["owner_id"] != 0)) {
				$query = "select * from business where id=%d and organisation_id=%d";
				if ($this->db->execute($query, $information["owner_id"], $this->user->organisation_id) == false) {
					$this->view->add_message("Owner does not exist.");
					$result = false;
				}
			}

			return $result;
		}

		private function save_labels($labels, $information_id) {
			if (is_array($labels) == false) {
				return true;
			}

			if (($label_ids = $this->borrow("label")->get_label_ids()) === false) {
				return false;
			}

			foreach ($labels as $label_id) {
				if (in_array($label_id, $label_ids) == false) {
					$this->user->log_action("unauthorized label assign attempt %d", $label_id);
					return false;
				}

				$value = array(
					"label_id"       => $label_id,
					"information_id" => $information_id);
				if ($this->db->insert("label_information", $value) === false) {
					return false;
				}
			}

			return true;
		}

		public function create_information($information) {
			$keys = array("id", "organisation_id", "name", "description", "owner_id", "confidentiality", "integrity", "availability");

			$information["id"] = null;
			$information["name"] = trim($information["name"]);
			$information["organisation_id"] = $this->user->organisation_id;

			if ($this->db->query("begin") === false) {
				return false;
			}

			if (($information["owner_id"] = $this->get_owner_id($information)) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->db->insert("information", $information, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}
			$information_id = $this->db->last_insert_id;

			if ($this->save_labels($information["labels"] ?? null, $this->db->last_insert_id) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->db->query("commit") === false) {
				return false;
			}

			return $information_id;
		}

		public function update_information($information) {
			$keys = array("name", "description", "owner_id", "confidentiality", "integrity", "availability");

			$information["name"] = trim($information["name"]);

			if ($this->db->query("begin") === false) {
				return false;
			}

			if (($information["owner_id"] = $this->get_owner_id($information)) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->db->update("information", $information["id"], $information, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			$query = "delete from label_information where information_id=%d";
			if ($this->db->query($query, $information["id"]) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->save_labels($information["labels"] ?? null, $information["id"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($information) {
			$result = true;

			if ($this->get_information($information["id"]) == false) {
				$this->view->add_message("Information not found.");
				$this->user->log_action("unauthorized delete attempt of information %d", $information["id"]);
				$result = false;
			}

			$query = "select * from processings where information_id=%d";
			if ($this->db->execute($query, $information["id"]) != false) {
				$this->view->add_message("Can't delete this information, because a processing uses it.");
				$result = false;
			}

			return $result;
		}

		public function delete_information($information_id) {
			$queries = array(
				array("delete from view_information where information_id=%d", $information_id),
				array("delete from information_application where information_id=%d", $information_id),
				array("delete from label_information where information_id=%d", $information_id),
				array("delete from information where id=%d", $information_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
