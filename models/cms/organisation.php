<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class cms_organisation_model extends Banshee\model {
		public function count_organisations() {
			$query = "select count(*) as count from organisations";

			if (($result = $this->db->execute($query)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_organisations($offset, $limit) {
			$query = "select * from organisations order by name limit %d,%d";

			return $this->db->execute($query, $offset, $limit);
		}

		public function get_organisation($organisation_id) {
			return $this->db->entry("organisations", $organisation_id);
		}

		public function get_users($organisation_id) {
			$query = "select * from users where organisation_id=%d order by fullname";

			return $this->db->execute($query, $organisation_id);
		}

		public function save_okay($organisation) {
			$result = true;

			if (trim($organisation["name"]) == "") {
				$this->view->add_message("Empty name is not allowed.");
				$result = false;
			}

			if (($check = $this->db->entry("organisations", $organisation["name"], "name")) === false) {
				$this->view->add_message("Database error.");
				$result = false;
			} else if ($check != false) {
				if ($check["id"] != $organisation["id"]) {
					$this->view->add_message("Organisation name already exists.");
					$result = false;
				}
			}

			return $result;
		}

		public function create_organisation($organisation) {
			$keys = array("id", "name");
			$organisation["id"] = null;

			return $this->db->insert("organisations", $organisation, $keys);
		}

		public function update_organisation($organisation) {
			$keys = array("name");

			return $this->db->update("organisations", $organisation["id"], $organisation, $keys);
		}

		public function delete_organisation($organisation_id) {
			$queries = array(
				/* Labels
				 */
				array("delete from label_application where application_id in ".
					"(select id from applications where organisation_id=%d)", $organisation_id),
				array("delete from label_business where business_id in ".
					"(select id from business where organisation_id=%d)", $organisation_id),
				array("delete from label_information where information_id in ".
					"(select id from information where organisation_id=%d)", $organisation_id),
				array("delete from labels where category_id in ".
					"(select id from label_categories where organisation_id=%d)", $organisation_id),
				array("delete from label_categories where organisation_id=%d", $organisation_id),
				/* Views
				 */
				array("delete from view_application where application_id in ".
					"(select id from applications where organisation_id=%d)", $organisation_id),
				array("delete from view_business where business_id in ".
					"(select id from business where organisation_id=%d)", $organisation_id),
				array("delete from view_file_operation where file_operation_id in ".
					"(select id from file_operations where application_id in ".
						"(select id from applications where organisation_id=%d))", $organisation_id),
				array("delete from view_hardware where hardware_id in ".
					"(select id from hardware where organisation_id=%d)", $organisation_id),
				array("delete from view_information where information_id in ".
					"(select id from information where organisation_id=%d)", $organisation_id),
				array("delete from view_frames where view_id in ".
					"(select id from views where organisation_id=%d)", $organisation_id),
				array("delete from views where organisation_id=%d", $organisation_id),
				/* Processings
				 */
				array("delete from processings where information_id in ".
					"(select id from information where organisation_id=%d)", $organisation_id),
				/* Links
				 */
				array("delete from application_business where application_id in ".
					"(select id from applications where organisation_id=%d)", $organisation_id),
				array("delete from application_hardware where application_id in ".
					"(select id from applications where organisation_id=%d)", $organisation_id),
				array("delete from connections where from_application_id in ".
					"(select id from applications where organisation_id=%d)", $organisation_id),
				array("delete from file_operations where application_id in ".
					"(select id from applications where organisation_id=%d)", $organisation_id),
				array("delete from information_application where application_id in ".
					"(select id from applications where organisation_id=%d)", $organisation_id),
				/* Entities
				 */
				array("update applications set owner_id=null where organisation_id=%d", $organisation_id),
				array("update information set owner_id=null where organisation_id=%d", $organisation_id),
				array("delete from applications where organisation_id=%d", $organisation_id),
				array("delete from business where organisation_id=%d", $organisation_id),
				array("delete from hardware where organisation_id=%d", $organisation_id),
				array("delete from information where organisation_id=%d", $organisation_id),
				/* Users
				 */
				array("delete from user_role where user_id in ".
					"(select id from users where organisation_id=%d)", $organisation_id),
				array("delete from users where organisation_id=%d", $organisation_id),
				array("delete from organisations where id=%d", $organisation_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
