/*!999999\- enable the sandbox mode */ 
-- MariaDB dump 10.19  Distrib 10.6.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ravib_iatlas
-- ------------------------------------------------------
-- Server version	10.6.18-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application_business`
--

DROP TABLE IF EXISTS `application_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_business` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL,
  `business_id` int(10) unsigned NOT NULL,
  `input` text NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `business_id` (`business_id`),
  CONSTRAINT `application_business_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  CONSTRAINT `application_business_ibfk_2` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_hardware`
--

DROP TABLE IF EXISTS `application_hardware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_hardware` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL,
  `hardware_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `hardware_id` (`hardware_id`),
  CONSTRAINT `application_hardware_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  CONSTRAINT `application_hardware_ibfk_2` FOREIGN KEY (`hardware_id`) REFERENCES `hardware` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `description` mediumtext NOT NULL,
  `owner_id` int(10) unsigned DEFAULT NULL,
  `location` tinyint(3) unsigned NOT NULL,
  `internet` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organisation_id` (`organisation_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `applications_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `applications_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `business`
--

DROP TABLE IF EXISTS `business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organisation_id` (`organisation_id`),
  CONSTRAINT `business_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `key` varchar(100) NOT NULL,
  `value` longtext NOT NULL,
  `timeout` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `connections`
--

DROP TABLE IF EXISTS `connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_application_id` int(10) unsigned NOT NULL,
  `to_application_id` int(10) unsigned NOT NULL,
  `protocol` text NOT NULL,
  `format` text NOT NULL,
  `frequency` text NOT NULL,
  `data_flow` tinyint(3) unsigned NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `from_application_id` (`from_application_id`),
  KEY `to_application_id` (`to_application_id`),
  CONSTRAINT `connections_ibfk_1` FOREIGN KEY (`from_application_id`) REFERENCES `applications` (`id`),
  CONSTRAINT `connections_ibfk_2` FOREIGN KEY (`to_application_id`) REFERENCES `applications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `file_operations`
--

DROP TABLE IF EXISTS `file_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_operations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL,
  `location` text NOT NULL,
  `format` text NOT NULL,
  `frequency` text NOT NULL,
  `data_flow` tinyint(4) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  CONSTRAINT `file_operations_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hardware`
--

DROP TABLE IF EXISTS `hardware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hardware` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `os` text NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `information`
--

DROP TABLE IF EXISTS `information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `description` mediumtext NOT NULL,
  `owner_id` int(10) unsigned DEFAULT NULL,
  `confidentiality` tinyint(3) unsigned NOT NULL,
  `availability` tinyint(3) unsigned NOT NULL,
  `integrity` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `organisation_id` (`organisation_id`),
  CONSTRAINT `information_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `information_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `information_application`
--

DROP TABLE IF EXISTS `information_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_application` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `information_id` int(10) unsigned NOT NULL,
  `application_id` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `information_id` (`information_id`),
  CONSTRAINT `information_application_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`),
  CONSTRAINT `information_application_ibfk_2` FOREIGN KEY (`information_id`) REFERENCES `information` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `label_application`
--

DROP TABLE IF EXISTS `label_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label_application` (
  `label_id` int(10) unsigned NOT NULL,
  `application_id` int(10) unsigned NOT NULL,
  KEY `label_id` (`label_id`),
  KEY `application_id` (`application_id`),
  CONSTRAINT `label_application_ibfk_1` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`),
  CONSTRAINT `label_application_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `label_business`
--

DROP TABLE IF EXISTS `label_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label_business` (
  `label_id` int(10) unsigned NOT NULL,
  `business_id` int(10) unsigned NOT NULL,
  KEY `label_id` (`label_id`),
  KEY `business_id` (`business_id`),
  CONSTRAINT `label_business_ibfk_1` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`),
  CONSTRAINT `label_business_ibfk_2` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `label_categories`
--

DROP TABLE IF EXISTS `label_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organisation_id` (`organisation_id`),
  CONSTRAINT `label_categories_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `label_information`
--

DROP TABLE IF EXISTS `label_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label_information` (
  `label_id` int(10) unsigned NOT NULL,
  `information_id` int(10) unsigned NOT NULL,
  KEY `label_id` (`label_id`),
  KEY `information_id` (`information_id`) USING BTREE,
  CONSTRAINT `label_information_ibfk_1` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`),
  CONSTRAINT `label_information_ibfk_2` FOREIGN KEY (`information_id`) REFERENCES `information` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `labels`
--

DROP TABLE IF EXISTS `labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `labels_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `label_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `javascript` tinyint(1) NOT NULL,
  `en` text NOT NULL,
  `nl` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page` (`module`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--
-- ORDER BY:  `id`

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'overview','overview',0,'Overview','Overzicht'),(2,'*','menu_overview',0,'Overview','Overzicht'),(3,'*','menu_labels',0,'Labels','Labels'),(4,'*','menu_processing',0,'Processing','Verwerkingen'),(5,'*','menu_lists',0,'Lists','Lijsten'),(6,'*','menu_views',0,'Views','Weergaven'),(7,'*','menu_search',0,'Search','Zoeken'),(8,'*','menu_manual',0,'Manual','Handleiding'),(9,'*','menu_logout',0,'Logout','Uitloggen'),(10,'*','information',0,'Information','Informatie'),(11,'*','applications',0,'Applications','Applicaties'),(12,'*','business',0,'Business','Business'),(13,'overview','total',0,'Total','Totaal'),(14,'overview','filter',0,'Filter','Filter'),(15,'*','labels',0,'Labels','Labels'),(16,'*','btn_back',0,'Back','Terug'),(17,'label','name',0,'Name','Naam'),(18,'*','name',0,'Name','Naam'),(19,'*','owner',0,'Owner','Eigenaar'),(20,'*','security_need',0,'Security need','Beveiligingsbehoefte'),(21,'information','all_information',0,'All information','Alle informatie'),(22,'*','storage_type',0,'Storage type','Opslagtype'),(23,'information','processing',0,'Processing','Verwerking'),(24,'information','role',0,'Role','Rol'),(25,'*','confidentiality',0,'Confidentiality','Vertrouwelijkheid'),(26,'*','integrity',0,'Integrity','Integriteit'),(27,'*','availability',0,'Availability','Beschikbaarheid'),(28,'application','type',0,'Type','Type'),(29,'*','location',0,'Location','Locatie'),(30,'application','internet_facing',0,'Internet facing','Gekoppeld aan het internet'),(31,'*','yes',0,'yes','ja'),(32,'*','no',0,'no','nee'),(33,'application','connections',0,'Connections','Verbindingen'),(34,'*','file_operations',0,'File operations','Bestandsoperaties'),(35,'application','used_by',0,'Used by','Gebruikt door'),(36,'application','runs_at',0,'Runs at','Draait op'),(37,'*','operating_system',0,'Operating system','Operating systeem'),(38,'*','information_input',0,'Information input','Informatie-invoer'),(39,'*','from',0,'From','Vanaf'),(40,'*','to',0,'To','Naar'),(41,'*','protocol',0,'Protocol','Protocol'),(42,'*','format',0,'Format','Formaat'),(43,'*','frequency',0,'Frequency','Freqentie'),(44,'*','data_flow',0,'Data flow','Datastroom'),(45,'application','all_applications',0,'All applications','Alle applicaties'),(46,'business','all_business',0,'All business entities','Alle business entiteiten'),(47,'business','description',0,'Description','Omschrijving'),(48,'business','application_usage',0,'Application usage','Applicatiegebruik'),(49,'business','application_ownership',0,'Application ownership','Applicatie-eigenaarschap'),(50,'business','information_input',0,'Information input','Informatie-invoer'),(51,'*','hardware',0,'Hardware','Hardware'),(52,'hardware','all_hardware',0,'All hardware','Alle hardware'),(53,'view','locked',0,'Locked','Vergrendeld'),(54,'view','btn_settings',0,'Settings','Instellingen'),(55,'view','btn_print',0,'Print','Printen'),(56,'view','btn_add_frame',0,'Add frame','Frame toevoegen'),(57,'hardware','file_operations',0,'File operations','Bestandsoperaties'),(58,'view','view',0,'view','bekijken'),(59,'view','views',0,'Views','Weergaven'),(60,'view','btn_new_view',0,'New view','Nieuwe weergave'),(61,'*','btn_cancel',0,'Cancel','Afbreken'),(62,'view','btn_save_view',0,'Save view','Weergave opslaan'),(63,'view','btn_delete_view',0,'Delete view','Weergave verwijderen'),(64,'search','search',0,'Search ','Zoeken'),(65,'search','sections',0,'Sections','Secties'),(66,'search','btn_search',0,'Search','Zoeken'),(67,'search','error_query_too_short',0,'Search query too short.','Zoekopdracht is te kort.'),(68,'search','error_search_error',0,'Search error.','Zoekfout.'),(69,'search','error_no_matches_found',0,'No matches found.','Geen resultaten gevonden.'),(70,'list','protocol',0,'Protocol','Protocol'),(71,'list','lists',0,'Lists','Lijsten'),(72,'list','from_application',0,'From application','Vanaf applicatie'),(73,'list','to_application',0,'To application','Naar applicatie'),(74,'list','input',0,'Input','Invoer'),(75,'list','at_application',0,'At application','In applicatie'),(76,'list','by_business',0,'By business entity','Door business-entiteit'),(77,'list','application',0,'Application','Applicatie'),(78,'processing','processing',0,'Processing','Verwerkingen'),(79,'processing','btn_export_to_csv',0,'Export to CSV','Exporteer naar CSV'),(81,'*','role',0,'Role','Rol'),(82,'processing','as',0,'as','als'),(83,'processing','btn_view_all',0,'View all','Toon alles'),(84,'*','application',0,'Application','Applicatie'),(85,'processing','processing_applications',0,'Applications processing this information','Applicaties die deze informatie verwerking'),(86,'processing','help_text',0,'<p>This page shows information about the processing of personal information, as required by article 30 of the General Data Protection Regulation (GDPR).</p>','<p>Op deze pagina vindt u informatie over de verwerking van persoonsgegevens, zoals vereist in artikel 30 van de Algemene Verordening Gegevensbescherming (AVG).</p>'),(87,'processing','security_measures',0,'Technical and organisational security measures','Technische en organisatorische beveiligingsmaatregelen'),(88,'processing','data_erasure',0,'Envisaged time limits for data erasure','Beoogde termijnen voor het wissen van gegevens'),(89,'processing','data_transfer',0,'Data transfer to third country or international organisation','Gegevensoverdracht naar derde land of internationale organisatie'),(90,'processing','recipients',0,'Categories of recipients','Categorieën van ontvangers'),(91,'processing','data_subjects',0,'Categories of data subjects and personal information','Categorieën van betrokkenen en persoonsgegevens'),(92,'processing','processing_purpose',0,'Purpose of the processing','Doel van de verwerking'),(93,'processing','processing_categories',0,'Categories of the processing carried out on behalf of controller','Categorieën van verwerkingen die namens de verwerkingsverantwoordelijke worden uitgevoerd'),(94,'processing','contact_controller',0,'Contact information of the controller','Contactgegevens van de verwerkingsverantwoordelijke'),(95,'processing','contact_processor',0,'Contact information of the processor and responsible controller','Contactgegevens van de verwerker en verantwoordelijke verwerkingsverantwoordelijke'),(96,'label','help_text',0,'<p>To each application and business entity, you can assign one or more labels. You can use the labels to categorize the applications and business entities. For example, of which process they are part of or the kind of information they process.</p>\r\n<p>The number to the right of a label indicates the number of objects (application or business entity) with that label.</p>','<p>Aan elke applicatie en bedrijfsentiteit kunt u een of meer labels toewijzen. U kunt de labels gebruiken om de applicaties en bedrijfsentiteiten te categoriseren. Bijvoorbeeld van welk proces ze deel uitmaken of het soort informatie dat ze verwerken.</p>\r\n<p>Het getal rechts van een label geeft het aantal objecten (applicatie of bedrijfsentiteit) met dat label aan.</p>'),(97,'overview','help_text',0,'<p>This page shows an overview of all the information, applications and business entities in the organisation\'s landscape.</p>\r\n<p>The number to the right of an item indicates the number of links to that object.</p>\r\n','<p>Deze pagina toont een overzicht van alle informatie, applicaties en business-entiteiten in het landschap van de organisatie.</p>\r\n<p>Het getal rechts van een item geeft het aantal links naar dat object aan.</p>'),(98,'view','help_text',0,'<p>Hold the CTRL key before you start dragging an element to align it to a grid.</p>\r\n<p>When you hover the mouse cursor over an item in the view area, unselected items in de sidebar which have a connection to that view-item are highlighted yellow. The item you hovered over is highlighted red.</p>\r\n\r\n<h1>Legenda</h1>\r\n<h2>Elements</h2>\r\n<ul>\r\n<li>Purple element: information.</li>\r\n<li>Blue element: application.</li>\r\n<li>Orange element: file operation.</li>\r\n<li>Yellow element: business entity.</li>\r\n<li>Green element: hardware.</li>\r\n</ul>\r\n\r\n<h2>Lines</h2>\r\n<ul>\r\n<li>Grey line: connection between applications.</li>\r\n<li>Dashed line: application or information used by business entity.</li>\r\n<li>Dotted line: application or information owned by business entity.</li>\r\n<li>Green line: application running on hardware.</li>\r\n<li>Orange line: application performing file operation.</li>\r\n</ul>\r\n\r\n<h2>Arrows</h2>\r\n<ul>\r\n<li>Open: direction of connection.</li>\r\n<li>Solid: direction of data flow.</li>\r\n</ul>','<p>Houd de CTRL-toets ingedrukt voordat u een element gaat slepen om het uit te lijnen op een raster.</p>\r\n<p>Wanneer u de muiscursor over een item in het weergavegebied beweegt, worden niet-geselecteerde items in de zijbalk die een verbinding hebben met dat weergave-item geel gemarkeerd. Het item waar u overheen hebt geheven, is rood gemarkeerd.</p>\r\n\r\n<h1>Legenda</h1>\r\n<h2>Elementen</h2>\r\n<ul>\r\n<li>Paars element: informatie.</li>\r\n<li>Blauw element: toepassing.</li>\r\n<li>Oranje element: bestandsbewerking.</li>\r\n<li>Geel element: bedrijfsentiteit.</li>\r\n<li>Groen element: hardware.</li>\r\n</ul>\r\n\r\n<h2>Lijnen</h2>\r\n<ul>\r\n<li>Grijze lijn: verbinding tussen toepassingen.</li>\r\n<li>Gestreepte lijn: toepassing of informatie die door de bedrijfsentiteit wordt gebruikt.</li>\r\n<li>Gestippelde lijn: toepassing of informatie die eigendom is van de bedrijfsentiteit.</li>\r\n<li>Groene lijn: toepassing die op hardware wordt uitgevoerd.</li>\r\n<li>Oranje lijn: toepassing die bestandsbewerking uitvoert.</li>\r\n</ul>\r\n\r\n<h2>Pijlen</h2>\r\n<ul>\r\n<li>Openen: richting van verbinding.</li>\r\n<li>Vast: richting van de gegevensstroom.</li>\r\n</ul>'),(99,'banshee/login','login',0,'Login','Inloggen'),(100,'banshee/login','username',0,'Username','Gebruikersnaam'),(101,'banshee/login','password',0,'Password','Wachtwoord'),(102,'banshee/login','authenticator_code',0,'Authenticator code','Authenticator-code'),(103,'banshee/login','only_when_enabled',0,'Only required when enabled for your account.','Alleen nodig indien ingeschakeld voor uw account.'),(104,'banshee/login','bind_to_ip',0,'Bind session to IP address','Verbind sessie aan IP-adres'),(105,'banshee/login','resubmit',0,'You tried to send data to this website while you are not logged in or your session was expired. If you want to resubmit that data, check the box below. If you reached this page via a web form on another website, don\'t login! An attacker possibly tries to change data in your account via this submit.','U hebt geprobeerd gegevens naar deze website te sturen terwijl u niet was ingelogd of uw sessie was verlopen. Als u die gegevens opnieuw wilt verzenden, vinkt u het onderstaande vakje aan. Als u deze pagina via een webformulier op een andere website hebt bereikt, log dan niet in! Een aanvaller probeert mogelijk gegevens in uw account te wijzigen via deze verzending.'),(106,'banshee/login','resend',0,'Resend data from previous submit','Gegevens opnieuw opsturen'),(107,'banshee/login','here',0,'here','hier'),(108,'banshee/login','forgot_password',0,'If you have forgotten your password, click','Indien u uw wachtwoord vergeten bent, klik dan'),(109,'banshee/login','click',0,'Click','Klik'),(110,'banshee/login','new_account',0,'to register for an account.','om een nieuw account aan te maken.'),(111,'logout','logout',0,'logout','Uitloggen'),(112,'logout','now_logged_out',0,'You are now logged out.','U bent nu uitgelogd.'),(113,'logout','not_logged_in',0,'You are not logged in.','U bent niet ingelogd.'),(114,'banshee/login','btn_login',0,'Login','Inloggen'),(115,'banshee/login','error_login_incorrect',0,'Login incorrect','Login onjuist'),(116,'session','session_manager',0,'Session manager','Sessiebeheer'),(117,'session','btn_logout',0,'Logout','Uitloggen'),(118,'session','name',0,'Name','Naam'),(119,'session','ip_address',0,'IP address','IP-adres'),(120,'session','expire_date',0,'Expire date','Vervaldatum'),(121,'session','ip_binded',0,'IP binded','IP gekoppeld'),(122,'*','are_you_sure',0,'Are you sure?','Weet u het zeker?'),(123,'session','btn_update_session',0,'Update session','Sessie bijwerken'),(124,'session','btn_delete_session',0,'Delete session','Sessie verwijderen'),(125,'session','error_session_not_found',0,'Session not found.','Sessie niet gevonden.'),(126,'session','error_session_delete',0,'Error while deleting session.','Fout bij verwijderen van de sessie.'),(127,'session','error_session_update',0,'Error while updating session.','Fout bij het bijwerken van de sessie.'),(128,'session','error_session_name_too_long',0,'Session name too long.','Sessienaam te lang.'),(129,'session','error_expire_date',0,'The expire time lies in the past','De vervaldatum ligt in het verleden.'),(130,'session','error_not_authenticated',0,'The session manager should not be accessible for non-authenticated visitors!','De sessiebeheerder mag niet toegankelijk zijn voor niet-geverifieerde bezoekers!'),(131,'*','footer_session_manager',0,'Session manager','Sessiebeheer'),(132,'*','footer_source_code',0,'Source code','Broncode'),(133,'*','footer_logged_in_as',0,'Logged in as','Ingelogd als'),(134,'account','name',0,'name','Naam'),(135,'account','organisation',0,'Organisation','Organisatie'),(136,'account','email_address',0,'E-mail address','E-mailadres'),(137,'account','password_current',0,'Current password','Huidige wachtwoord'),(138,'account','password_new',0,'New password','Nieuwe wachtwoord'),(139,'account','password_repeat',0,'Repeat password','Herhaal wachtwoord'),(140,'account','authenticator_secret',0,'Authenticator secret','Authenticatorgeheim'),(141,'account','not_changed_when_blank',0,'will not be changed when left blank','wordt niet gewijzigd indien leeggelaten'),(142,'account','recent_account_activity',0,'Recent account activity','Recentelijke account activiteit'),(143,'account','ip_address',0,'IP address','IP-adres'),(144,'account','timestamp',0,'Timestamp','Tijdstempel'),(145,'account','activity',0,'Activity','Activiteit'),(146,'account','btn_logout',0,'Logout','Uitloggen'),(147,'account','btn_account_delete',0,'Delete account','Verwijder account'),(148,'account','btn_account_update',0,'Update acccount','Account bijwerken'),(149,'account','account',0,'Account','Account'),(150,'account','error_not_logged_in',0,'You are not logged in.','U bent niet ingelogd.'),(151,'account','change_password',0,'Please, change your password.','Verander aub uw wachtwoord.'),(152,'account','account_deleted_logged_out',0,'Your account has been deleted. You are now logged out.','Uw account is verwijderd. U ben nu uitgelogd.'),(153,'account','error_account_delete',0,'Something went wrong while deleting this account.','Er ging iets fout tijdens het verwijderen van dit account.'),(154,'account','error_account_update',0,'Error while updating account.','Fout tijdens het bijwerken van het account.'),(155,'account','account_updated',0,'Account has been updated.','Uw account is bijgewerkt.'),(156,'*','error_database',0,'Database error.','Databasefout.'),(157,'*','error_name_empty',0,'Fill in your name.','Vul uw naam in.'),(158,'*','error_email_exists',0,'E-mail address already exists.','E-mailadres al in gebruik.'),(159,'*','error_password_too_long',0,'Current password is too long.','Hudiige wachtwoord is te lang.'),(160,'*','error_password_incorrect',0,'Current password is incorrect.','Huidige wachtwoord is onjuist.'),(161,'*','error_password_no_match',0,'New passwords do not match.','Nieuwe wachtwoorden komen niet overeen.'),(162,'*','error_password_same',0,'New password must be different from your current password.','Het nieuwe wachtwoord mag niet gelijk zijn aan het huidige wachtwoord.'),(163,'password','forgot_password',0,'Forgot password','Wachtwoord vergeten'),(164,'password','password',0,'Password','Wachtwoord'),(165,'password','repeat',0,'Repeat','Heraal'),(166,'password','enter_new_password',0,'Enter a new password for your account.','Voer een nieuw wachtwoord in voor uw account.'),(167,'password','username',0,'Username','Gebruikersnaam'),(168,'password','email_address',0,'E-mail address','E-mailadres'),(169,'password','btn_reset_password',0,'Reset password','Wachtwoord opnieuw instellen'),(170,'password','btn_save_password',0,'Save password','Wachtwoord opslaan'),(171,'password','enter_username_and_email',0,'Enter your username and e-mail address to reset your password.','Voer uw gebruikersnaam en e-mailadres in om uw wachtwoord opnieuw in te stellen.'),(172,'password','link_sent',0,'<p>If you have entered an existing username and e-mail address, a link to reset your password has been sent to the supplied e-mail address.</p>\r\n<p>Don\'t close your browser!!</p>','<p>Als u een bestaande gebruikersnaam en e-mailadres hebt ingevoerd, is er een link om uw wachtwoord opnieuw in te stellen naar het opgegeven e-mailadres verzonden.</p>\r\n<p>Uw browser niet sluiten!!</p>'),(173,'password','error_passwords_not_same',0,'The passwords are not the same.','De wachtwoorden komen niet overeen.'),(174,'password','reset_password_at',0,'Reset password at','Wachtwoord opnieuw instellen voor'),(175,'password','password_saved',0,'The password has been saved.','Het wachtwoord is opgeslagen.'),(176,'password','error_saving_password',0,'Error while saving password.','Fout bij het opslaan van het wachtwoord.'),(177,'*','btn_previous',0,'Previous <<','Vorige <<'),(178,'*','btn_next',0,'Next >>','Volgende >>'),(179,'*','btn_submit',0,'Submit','Opsturen'),(180,'register','register',0,'Register','Registreren'),(181,'register','btn_register',0,'Register','Registreren'),(182,'register','email_address',0,'E-mail address','E-mailadres'),(183,'register','verification_code',0,'Verification code','Verificatiecode'),(184,'register','verification_code_sent',0,'An e-mail with a verification code has been sent to your e-mail address.','Er is een e-mail met een verificatiecode naar uw e-mailadres verzonden.'),(185,'register','full_name',0,'Full name','Volledige naam'),(186,'register','username',0,'Username','Gebruikersnaam'),(187,'register','password',0,'Password','Wachtwoord'),(188,'register','organisation',0,'Organisation','Organisatie'),(189,'register','account_created',0,'Your account has been created. You can now log in.','Uw account is aangemaakt. U kunt nu inloggen.'),(190,'register','already_have_account',0,'You already have an account.','U heeft reeds een account.'),(191,'register','verification_code_for',0,'Verification code for','Verificatiecode voor'),(192,'connection','connection',0,'Connection','Verbinding'),(193,'fileoperation','file_operation',0,'File operation','Bestandsoperatie'),(194,'*','description',0,'Description','Omschrijving'),(196,'*','used_by',0,'Used by','Gebruikt door'),(197,'account','authenticator_help',0,'<p>This option requires the use of an authenticator app (RFC 6238) on your mobile phone.</p>\r\n<p>The app must use BASE32 characters, SHA1 and a 30 second time interval to generate a 6 digit code.</p>','<p>Voor deze optie is het gebruik van een authenticator-app (RFC 6238) op uw mobiele telefoon vereist.</p>\r\n<p>De app moet BASE32-tekens, SHA1 en een tijdsinterval van 30 seconden gebruiken om een ​​6-cijferige code te genereren.</p>'),(198,'account','error_email_invalid',0,'Invalid e-mail address','Ongeldig e-mailadres'),(199,'account','error_authenticator_invalid',0,'Invalid authenticator secret','Ongeldig authenticatorgeheium'),(200,'view','error_view_not_found',0,'View not found.','Weergave niet gevonden.'),(201,'view','error_specify_name',0,'Specify the view\'s name.','Specificeer de naam van de weergave.'),(202,'register','error_email_invalid',0,'Invalid e-mail address.','Ongeldig e-mailadres.'),(203,'register','error_email_used',0,'The e-mail address has already been used to register an account.','Het e-mailadres is al in gebruik bij een ander account.'),(204,'register','error_verification_code_invalid',0,'Invalid verification code.','Ongeldige verificatiecode.'),(205,'register','error_organisation_used',0,'The organisation name is already taken.','De organisatienaam is al in gebruik.'),(206,'register','error_username_taken',0,'The username is already taken.','De gebruikersnaam is al in gebruik.'),(207,'register','error_organisation_empty',0,'Fill in the organisation name.','Vul de organisatienaam in.'),(208,'register','error_username_length',0,'The length of your name must be equal or greater than %d.','De lengte van je naam moet gelijk zijn aan of groter zijn dan %d.'),(209,'register','error_username_case_length',0,'Your username must consist of lowercase letters with a mimimum length of %d.','De gebruikersnaam moet bestaan uit kleine letters en moet een minimale lengte hebben van %d.'),(210,'*','redirect_1',0,'Click','Klik'),(211,'*','redirect_2',0,'here','hier'),(212,'*','redirect_3',0,'to continue or wait','om door te gaan of wacht'),(213,'*','redirect_4',0,'seconds to be redirected.','seconden om doorgestuurd te worden.'),(214,'banshee/error','error',0,'Error','Fout'),(215,'*','help_classification',0,'<li><b>Confidentiality:</b><ul>\r\n    <li><b>Public:</b> The information may be viewed by everybody.</li>\r\n    <li><b>Internal:</b> The information may only be viewed by own employees and a select number of business partners.</li>\r\n    <li><b>Confidential:</b> The information may only be viewed by a select number of own employees. A breach of the confidentiality has a serious impact on the organization. Confidentiality may be enforced by (privacy) law.</li>\r\n    <li><b>Secret:</b> The information may only be viewed by a select number of own employees. A breach of the confidentiality has a serious impact on the organization, but also other organisations. Confidentiality may be enforced by law (state secrets).</li>\r\n</ul></li>\r\n<li><b>Integrity</b><ul>\r\n    <li><b>Normal:</b> The information is not necessary for the execution of a primary process.</li>\r\n    <li><b>Important:</b> The integrity of the information is necessary for the execution of a primary process.</li>\r\n    <li><b>Crucial:</b> The integrity of the information is necessary for the execution of a primary process and must be provable.</li>\r\n</ul></li>\r\n<li><b>Availability:</b><ul>\r\n    <li><b>Normal:</b> The information and/or the system is not necessary for the execution of a primary process.</li>\r\n    <li><b>Important:</b> The information and/or the system is necessary to a limited extent for the execution of a primary process.</li>\r\n    <li><b>Crucial:</b>The information and/or the system is indispensable for the execution of a primary process.</li>\r\n</ul></li>','<li><b>Vertrouwelijkheid:</b><ul>\r\n<li><b>Openbaar:</b> De informatie mag door iedereen worden ingezien.</li>\r\n<li><b>Intern:</b> De informatie mag alleen door eigen medewerkers en een select aantal zakenpartners worden ingezien.</li>\r\n<li><b>Vertrouwelijk:</b> De informatie mag alleen door een select aantal eigen medewerkers worden ingezien. Een schending van de vertrouwelijkheid heeft ernstige gevolgen voor de organisatie. Vertrouwelijkheid kan worden afgedwongen door de (privacy)wetgeving.</li>\r\n<li><b>Geheim:</b> De informatie mag alleen door een select aantal eigen medewerkers worden ingezien. Een schending van de vertrouwelijkheid heeft ernstige gevolgen voor de organisatie, maar ook voor andere organisaties. Vertrouwelijkheid kan worden afgedwongen door de wet (staatsgeheimen).</li>\r\n</ul></li>\r\n<li><b>Integriteit</b><ul>\r\n<li><b>Normaal:</b> De informatie is niet nodig voor de uitvoering van een primair proces.</li>\r\n<li><b>Belangrijk:</b> De integriteit van de informatie is nodig voor de uitvoering van een primair proces.</li>\r\n<li><b>Cruciaal:</b> De integriteit van de informatie is nodig voor de uitvoering van een primair proces en moet aantoonbaar zijn.</li>\r\n</ul></li>\r\n<li><b>Beschikbaarheid:</b><ul>\r\n<li><b>Normaal:</b> De informatie en/of het systeem is niet nodig voor de uitvoering van een primair proces.</li>\r\n<li><b>Belangrijk:</b> De informatie en/of het systeem is in beperkte mate nodig voor de uitvoering van een primair proces.</li>\r\n<li><b>Cruciaal:</b> De informatie en/of het systeem is onmisbaar voor de uitvoering van een primair proces.</li>\r\n</ul></li>'),(216,'application','help_location',0,'li><b>Location:</b><ul>\r\n    <li><b>Internal:</b> The application is hosted and maintained by the organisation self.</li>\r\n    <li><b>External:</b> The application is hosted by a third-party organisation, but maintained by the organisation self.</li>\r\n    <li><b>SAAS:</b> The application is hosted and maintained by a third-party organisation.</li>\r\n</ul></li>','<li><b>Locatie:</b><ul>\r\n<li><b>Intern:</b> De applicatie wordt gehost en onderhouden door de organisatie zelf.</li>\r\n<li><b>Extern:</b> De applicatie wordt gehost door een externe organisatie, maar onderhouden door de organisatie zelf.</li>\r\n<li><b>SAAS:</b> De applicatie wordt gehost en onderhouden door een externe organisatie.</li>\r\n</ul></li>'),(217,'view','error_width_invalid',0,'Invalid width.','Ongeldige breedte.'),(218,'view','error_height_invalid',0,'Invalid height.','Ongeldige hoogte.'),(219,'view','width',0,'Width','Breedte'),(220,'view','height',0,'Height','Hoogte'),(221,'view','size',0,'Size','Grootte');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `text` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--
-- ORDER BY:  `id`

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,NULL,'menu_overview','/overview'),(2,NULL,'menu_labels','/label'),(3,NULL,'menu_processing','/processing'),(4,NULL,'menu_lists','/list'),(5,NULL,'menu_views','/view'),(6,NULL,'menu_search','/search'),(7,NULL,'menu_manual','/manual'),(8,NULL,'menu_logout','/logout');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisations`
--

DROP TABLE IF EXISTS `organisations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisations`
--
-- ORDER BY:  `id`

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;
INSERT INTO `organisations` VALUES (1,'My organisation');
/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_access`
--

DROP TABLE IF EXISTS `page_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_access` (
  `page_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`page_id`,`role_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_access`
--
-- ORDER BY:  `page_id`,`role_id`

LOCK TABLES `page_access` WRITE;
/*!40000 ALTER TABLE `page_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `language` varchar(2) NOT NULL,
  `layout` varchar(100) DEFAULT NULL,
  `private` tinyint(1) NOT NULL,
  `style` mediumtext DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `back` tinyint(1) NOT NULL,
  `form` tinyint(1) NOT NULL,
  `form_submit` varchar(32) DEFAULT NULL,
  `form_email` varchar(100) DEFAULT NULL,
  `form_done` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--
-- ORDER BY:  `id`

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'/manual','en',NULL,0,'img.scheme {\r\n	width:100%;\r\n}\r\n\r\nimg.icon {\r\n	float:left;\r\n	margin-right:10px;\r\n	margin-top:10px;\r\n}\r\n\r\nh3 ~ p {\r\n	margin-left:60px;\r\n}\r\n\r\nh2 + p {\r\n	margin-left:0;\r\n}','Manual','','','<p>This tool allows you to create an overview of all information within your organisation and share it with colleagues. An overview is created by specifying entities (the information itself and things related to that information) and by linking them together. The following entities are available in this tool:</p>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<ul>\r\n<li><b style=\"background-color:#cfc4ff\">Information:</b> The most important entity is information. It\'s why this tool was built. Information is owned by a business entity and is stored in or processed by an application.</li>\r\n<li><b style=\"background-color:#faffa7\">Business:</b> A business entity is an information or application owner (ownership is shown as a dotted line) or an application user (usage is shown as a dashed line).</li>\r\n<li><b style=\"background-color:#cbeaae\">Hardware:</b> A piece of hardware is a (virtual) device that hosts one or more applications.</li>\r\n<li><b style=\"background-color:#c7feff\">Application:</b> An application is software that contains or processes information. It is being used by a business entity and is hosted on hardware. It can exchange information with other applications.</li>\r\n<li><b style=\"background-color:#fbb875\">File operation:</b> A file operation means a file that is created, updated or read by an application.</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-6\">\r\n<img src=\"/images/scheme.png\" class=\"scheme\" />\r\n</div>\r\n</div>\r\n\r\n<h2>Application modules</h2>\r\n<p>This tool consists of several modules, which can be accessed via the menu bar at the top of the screen.</p>\r\n\r\n<img src=\"/images/icons/overview.png\" class=\"icon\" />\r\n<h3>Overview</h3>\r\n<p>The overview shows all information, business entities and applications that have been defined in the landscape. The number tag shows the number of links to other entities. From here, you can see the details of all entities by clicking on an entity or link.</p>\r\n<p>The confidentiality, integrity and availability classification of an application is derived from the classification of the information stored in that application.</p>\r\n\r\n<img src=\"/images/icons/labels.png\" class=\"icon\" />\r\n<h3>Labels</h3>\r\n<p>This section shows all the labels groups and the labels within those groups that have been used to categorize all the entities.</p>\r\n\r\n<img src=\"/images/icons/processing.png\" class=\"icon\" />\r\n<h3>Processing</h3>\r\n<p>The processing of personal information, as defined in the article 30 of the GDPR, can be viewed here.</p>\r\n\r\n<img src=\"/images/icons/list.png\" class=\"icon\" />\r\n<h3>Lists</h3>\r\n<p>The information with all the entities can filtered and sorted in many interesting ways. This modules let\'s you take a different look at the information within the entities via several pre-defined lists.</p>\r\n\r\n<img src=\"/images/icons/views.png\" class=\"icon\" />\r\n<h3>Views</h3>\r\n<p>A view is a graphical representation of a user defined selection of the entities. Multiple views can be made and saved.</p>\r\n\r\n<img src=\"/images/icons/search.png\" class=\"icon\" />\r\n<h3>Search</h3>\r\n<p>The search module allows you to search for any text within the entities.</p>\r\n\r\n<h2>Content Management System</h2>\r\n<p>To start creating such overview, click on the <a href=\"/cms\">CMS link</a> in the bar at the bottom of this page. That will bring you to the Content Management System of this tool. There you will find several options to create and maintain your information overview.</p>\r\n\r\n<img src=\"/images/icons/labels.png\" class=\"icon\" />\r\n<h3>Labels</h3>\r\n<p>Here you define labels and label categories. Labels can be assigned to any entity. Each label belongs to a label category. You can use labels to organize the entities within your information landscape. You can create labels for departments or processes or anything you want. Labels are clickable and clicking on a label will show a list with all entities to which that label has been assigned.</p>\r\n\r\n<img src=\"/images/icons/information.png\" class=\"icon\" />\r\n<h3>Information</h3>\r\n<p>The most important entity within this tool, information, is defined here.</p>\r\n\r\n<img src=\"/images/icons/business.png\" class=\"icon\" />\r\n<h3>Business</h3>\r\n<p>Here you define information and application owners and application users.</p>\r\n\r\n<img src=\"/images/icons/hardware.png\" class=\"icon\" />\r\n<h3>Hardware</h3>\r\n<p>Here you specify (virtual) devices that host applications. You can use this to quickly see if one hacked application can affect the security of other applications, those running on the same hardware.</p>\r\n\r\n<img src=\"/images/icons/application.png\" class=\"icon\" />\r\n<h3>Applications</h3>\r\n<p>Although information is the most important entity, an application is what links all entities together. In this section, you can specify the  applications, but here you will also find the \'Applications link\' button which brings you to the section where you can create the actual links with the other entities.</p>\r\n\r\n<img src=\"/images/icons/processing.png\" class=\"icon\" />\r\n<h3>Processing</h3>\r\n<p>Since you\'re already registering information, you might as well register the processing of personal information, as defined in <a href=\"https://www.privacy-regulation.eu/en/article-30-records-of-processing-activities-GDPR.htm\">article 30 of the GDPR</a>. This can be done in this section.</p>\r\n\r\n<img src=\"/images/icons/issues.png\" class=\"icon\" />\r\n<h3>Issues</h3>\r\n<p>Here you will find flaws that this tool has detected in your information landscape, such as information or applications without an owner, applications without hardware, unused hardware, etc.</p>\r\n\r\n<img src=\"/images/icons/data.png\" class=\"icon\" />\r\n<h3>Data</h3>\r\n<p>All data you enter in this tool can be exported for safe keeping and imported.</p>',1,0,0,NULL,NULL,NULL),(5,'/manual','nl',NULL,0,'img.scheme {\r\n	width:100%;\r\n}\r\n\r\nimg.icon {\r\n	float:left;\r\n	margin-right:10px;\r\n	margin-top:10px;\r\n}\r\n\r\nh3 ~ p {\r\n	margin-left:60px;\r\n}\r\n\r\nh2 + p {\r\n	margin-left:0;\r\n}','Handleiding','','','<p>Met deze tool kunt u een overzicht maken van alle informatie binnen uw organisatie en deze delen met collega\'s. Een overzicht wordt gemaakt door entiteiten (de informatie zelf en zaken die verband houden met die informatie) te specificeren en deze aan elkaar te koppelen. De volgende entiteiten zijn beschikbaar in deze tool:</p>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<ul>\r\n<li><b style=\"background-color:#cfc4ff\">Informatie:</b> De belangrijkste entiteit is informatie. Daarom is deze tool gebouwd. Informatie is eigendom van een bedrijfsentiteit en wordt opgeslagen in of verwerkt door een applicatie.</li>\r\n<li><b style=\"background-color:#faffa7\">Bedrijf:</b> Een bedrijfsentiteit is een eigenaar van informatie of een applicatie (eigendom wordt weergegeven als een stippellijn) of een applicatiegebruiker (gebruik wordt weergegeven als een stippellijn).</li>\r\n<li><b style=\"background-color:#cbeaae\">Hardware:</b> Een stuk hardware is een (virtueel) apparaat dat een of meer applicaties host.</li>\r\n<li><b style=\"background-color:#c7feff\">Applicatie:</b> Een applicatie is software die informatie bevat of verwerkt. Het wordt gebruikt door een bedrijfsentiteit en wordt gehost op hardware. Het kan informatie uitwisselen met andere applicaties.</li>\r\n<li><b style=\"background-color:#fbb875\">Bestandsbewerking:</b> Een bestandsbewerking betekent een bestand dat wordt gemaakt, bijgewerkt of gelezen door een applicatie.</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-6\">\r\n<img src=\"/images/scheme.png\" class=\"scheme\" />\r\n</div>\r\n</div>\r\n\r\n<h2>Applicatiemodules</h2>\r\n<p>Deze tool bestaat uit verschillende modules, die toegankelijk zijn via de menubalk boven aan het scherm.</p>\r\n\r\n<img src=\"/images/icons/overview.png\" class=\"icon\" />\r\n<h3>Overzicht</h3>\r\n<p>Het overzicht toont alle informatie, bedrijfsentiteiten en applicaties die in het landschap zijn gedefinieerd. De nummertag toont het aantal links naar andere entiteiten. Vanaf hier kunt u de details van alle entiteiten bekijken door op een entiteit of link te klikken.</p>\r\n<p>De vertrouwelijkheid, integriteit en beschikbaarheidsclassificatie van een applicatie is afgeleid van de classificatie van de informatie die in die applicatie is opgeslagen.</p>\r\n\r\n<img src=\"/images/icons/labels.png\" class=\"icon\" />\r\n<h3>Labels</h3>\r\n<p>In deze sectie worden alle labelgroepen en de labels binnen die groepen weergegeven die zijn gebruikt om alle entiteiten te categoriseren.</p>\r\n\r\n<img src=\"/images/icons/processing.png\" class=\"icon\" />\r\n<h3>Verwerking</h3>\r\n<p>De verwerking van persoonlijke informatie, zoals gedefinieerd in artikel 30 van de AVG, kan hier worden bekeken.</p>\r\n\r\n<img src=\"/images/icons/list.png\" class=\"icon\" />\r\n<h3>Lijsten</h3>\r\n<p>De informatie met alle entiteiten kan worden gefilterd en op veel interessante manieren gesorteerd. Met deze modules kunt u de informatie binnen de entiteiten op een andere manier bekijken via verschillende vooraf gedefinieerde lijsten.</p>\r\n\r\n<img src=\"/images/icons/views.png\" class=\"icon\" />\r\n<h3>Weergaven</h3>\r\n<p>Een weergave is een grafische weergave van een door de gebruiker gedefinieerde selectie van de entiteiten. Er kunnen meerdere weergaven worden gemaakt en opgeslagen.</p>\r\n\r\n<img src=\"/images/icons/search.png\" class=\"icon\" />\r\n<h3>Zoeken</h3>\r\n<p>Met de zoekmodule kunt u zoeken naar tekst binnen de entiteiten.</p>\r\n\r\n<h2>Content Management System</h2>\r\n<p>Om een ​​dergelijk overzicht te maken, klikt u op de <a href=\"/cms\">CMS-link</a> in de balk onderaan deze pagina. U komt dan in het Content Management System van deze tool. Daar vindt u verschillende opties om uw informatieoverzicht te maken en te onderhouden.</p>\r\n\r\n<img src=\"/images/icons/labels.png\" class=\"icon\" />\r\n<h3>Labels</h3>\r\n<p>Hier definieert u labels en labelcategorieën. Labels kunnen aan elke entiteit worden toegewezen. Elk label behoort tot een labelcategorie. U kunt labels gebruiken om de entiteiten binnen uw informatielandschap te ordenen. U kunt labels maken voor afdelingen of processen of wat u maar wilt. Labels zijn klikbaar en als u op een label klikt, wordt een lijst weergegeven met alle entiteiten waaraan dat label is toegewezen.</p>\r\n\r\n<img src=\"/images/icons/information.png\" class=\"icon\" />\r\n<h3>Information</h3>\r\n<p>De belangrijkste entiteit binnen deze tool, informatie, wordt hier gedefinieerd.</p>\r\n\r\n<img src=\"/images/icons/business.png\" class=\"icon\" />\r\n<h3>Business</h3>\r\n<p>Hier definieert u informatie en applicatie-eigenaren en applicatie-gebruikers.</p>\r\n\r\n<img src=\"/images/icons/hardware.png\" class=\"icon\" />\r\n<h3>Hardware</h3>\r\n<p>Hier specificeert u (virtuele) apparaten die applicaties hosten. U kunt dit gebruiken om snel te zien of een gehackte applicatie de beveiliging van andere applicaties kan beïnvloeden, die op dezelfde hardware draaien.</p>\r\n\r\n<img src=\"/images/icons/application.png\" class=\"icon\" />\r\n<h3>Applications</h3>\r\n<p>Hoewel informatie de belangrijkste entiteit is, is een applicatie wat alle entiteiten met elkaar verbindt. In deze sectie kunt u de applicaties specificeren, maar hier vindt u ook de knop \'Applications link\' die u naar de sectie brengt waar u de daadwerkelijke koppelingen met de andere entiteiten kunt maken.</p>\r\n\r\n<img src=\"/images/icons/processing.png\" class=\"icon\" />\r\n<h3>Processing</h3>\r\n<p>Aangezien u toch al informatie registreert, kunt u net zo goed de verwerking van persoonlijke informatie registreren, zoals gedefinieerd in <a href=\"https://www.privacy-regulation.eu/en/article-30-records-of-processing-activities-GDPR.htm\">artikel 30 van de AVG</a>. Dit kan in deze sectie.</p>\r\n\r\n<img src=\"/images/icons/issues.png\" class=\"icon\" />\r\n<h3>Issues</h3>\r\n<p>Hier vindt u gebreken die deze tool heeft gedetecteerd in uw informatielandschap, zoals informatie of applicaties zonder eigenaar, applicaties zonder hardware, ongebruikte hardware, etc.</p>\r\n\r\n<img src=\"/images/icons/data.png\" class=\"icon\" />\r\n<h3>Data</h3>\r\n<p>Alle gegevens die u in deze tool invoert, kunnen worden geëxporteerd voor veilige bewaring en geïmporteerd.</p>',1,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processings`
--

DROP TABLE IF EXISTS `processings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `information_id` int(10) unsigned NOT NULL,
  `description` varchar(100) NOT NULL,
  `role` tinyint(3) unsigned NOT NULL,
  `contact` mediumtext DEFAULT NULL,
  `purpose` mediumtext DEFAULT NULL,
  `subject` mediumtext DEFAULT NULL,
  `recipient` mediumtext DEFAULT NULL,
  `transfer` mediumtext DEFAULT NULL,
  `erasure` mediumtext DEFAULT NULL,
  `security` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `information_id` (`information_id`),
  CONSTRAINT `processings_ibfk_2` FOREIGN KEY (`information_id`) REFERENCES `information` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reroute`
--

DROP TABLE IF EXISTS `reroute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reroute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original` varchar(100) NOT NULL,
  `replacement` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `non_admins` smallint(6) NOT NULL,
  `cms` tinyint(1) NOT NULL,
  `cms/access` tinyint(1) NOT NULL,
  `cms/action` tinyint(1) NOT NULL,
  `cms/application` tinyint(1) NOT NULL,
  `cms/application/link` tinyint(1) NOT NULL,
  `cms/business` tinyint(1) NOT NULL,
  `cms/data` tinyint(1) NOT NULL,
  `cms/file` tinyint(1) NOT NULL,
  `cms/hardware` tinyint(1) NOT NULL,
  `cms/information` tinyint(1) NOT NULL,
  `cms/issues` tinyint(1) NOT NULL,
  `cms/label` tinyint(1) NOT NULL,
  `cms/label/category` tinyint(1) NOT NULL,
  `cms/menu` tinyint(1) NOT NULL,
  `cms/organisation` tinyint(1) NOT NULL,
  `cms/page` tinyint(1) NOT NULL,
  `cms/processing` tinyint(1) NOT NULL,
  `cms/role` tinyint(1) NOT NULL,
  `cms/settings` tinyint(1) NOT NULL,
  `cms/switch` tinyint(1) NOT NULL,
  `cms/translation` tinyint(1) NOT NULL,
  `cms/user` tinyint(1) NOT NULL,
  `cms/reroute` tinyint(1) NOT NULL,
  `account` tinyint(1) NOT NULL,
  `application` tinyint(1) NOT NULL,
  `business` tinyint(1) NOT NULL,
  `connection` tinyint(1) NOT NULL,
  `export` tinyint(1) NOT NULL,
  `fileoperation` tinyint(1) NOT NULL,
  `hardware` tinyint(1) NOT NULL,
  `information` tinyint(1) NOT NULL,
  `label` tinyint(1) NOT NULL,
  `list` tinyint(1) NOT NULL,
  `overview` tinyint(1) NOT NULL,
  `processing` tinyint(1) NOT NULL,
  `search` tinyint(1) NOT NULL,
  `session` tinyint(1) NOT NULL,
  `usedby` tinyint(1) NOT NULL,
  `view` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--
-- ORDER BY:  `id`

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator',0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,'Maintainer',1,1,1,0,1,1,1,1,0,1,1,1,1,1,0,0,0,1,0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(3,'User',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(4,'Data Protection Officer',1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,1,0,1,0,0);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(128) NOT NULL,
  `login_id` varchar(128) DEFAULT NULL,
  `content` mediumtext DEFAULT NULL,
  `expire` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `bind_to_ip` tinyint(1) NOT NULL,
  `name` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `type` varchar(8) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--
-- ORDER BY:  `id`

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'admin_page_size','integer','25'),(2,'database_version','float','1.2'),(3,'default_language','string','en'),(8,'head_description','string','iAtlas, the information landscape tool'),(9,'head_keywords','string','information landscape, information systems, applications, landscape'),(10,'head_title','string','iAtlas'),(11,'hiawatha_cache_default_time','integer','3600'),(12,'hiawatha_cache_enabled','boolean','false'),(27,'secret_website_code','string',''),(28,'session_persistent','boolean','true'),(29,'session_timeout','string','1 day'),(30,'start_page','string','overview'),(33,'webmaster_email','string','root@localhost'),(36,'max_apps_per_server','integer','2'),(37,'validate_import_signature','boolean','true');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `one_time_key` varchar(128) DEFAULT NULL,
  `cert_serial` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `authenticator_secret` varchar(16) DEFAULT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--
-- ORDER BY:  `id`

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','none',NULL,NULL,1,NULL,'Administrator','root@localhost');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_application`
--

DROP TABLE IF EXISTS `view_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_application` (
  `view_id` int(10) unsigned NOT NULL,
  `application_id` int(10) unsigned NOT NULL,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned NOT NULL,
  KEY `view_id` (`view_id`),
  KEY `application_id` (`application_id`),
  CONSTRAINT `view_application_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`),
  CONSTRAINT `view_application_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `view_business`
--

DROP TABLE IF EXISTS `view_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_business` (
  `view_id` int(10) unsigned NOT NULL,
  `business_id` int(10) unsigned NOT NULL,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned NOT NULL,
  KEY `view_id` (`view_id`),
  KEY `application_id` (`business_id`),
  CONSTRAINT `view_business_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`),
  CONSTRAINT `view_business_ibfk_2` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `view_file_operation`
--

DROP TABLE IF EXISTS `view_file_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_file_operation` (
  `view_id` int(10) unsigned NOT NULL,
  `file_operation_id` int(10) unsigned NOT NULL,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned NOT NULL,
  KEY `view_id` (`view_id`),
  KEY `application_id` (`file_operation_id`),
  CONSTRAINT `view_file_operation_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`),
  CONSTRAINT `view_file_operation_ibfk_2` FOREIGN KEY (`file_operation_id`) REFERENCES `file_operations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `view_frames`
--

DROP TABLE IF EXISTS `view_frames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_frames` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `view_id` int(10) unsigned NOT NULL,
  `title` tinytext NOT NULL,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `view_id` (`view_id`),
  CONSTRAINT `view_frames_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `view_hardware`
--

DROP TABLE IF EXISTS `view_hardware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_hardware` (
  `view_id` int(10) unsigned NOT NULL,
  `hardware_id` int(10) unsigned NOT NULL,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned NOT NULL,
  KEY `view_id` (`view_id`),
  KEY `application_id` (`hardware_id`),
  CONSTRAINT `view_hardware_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`),
  CONSTRAINT `view_hardware_ibfk_2` FOREIGN KEY (`hardware_id`) REFERENCES `hardware` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `view_information`
--

DROP TABLE IF EXISTS `view_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_information` (
  `view_id` int(10) unsigned NOT NULL,
  `information_id` int(10) unsigned NOT NULL,
  `x` int(10) unsigned NOT NULL,
  `y` int(10) unsigned NOT NULL,
  KEY `view_id` (`view_id`),
  KEY `application_id` (`information_id`),
  CONSTRAINT `view_information_ibfk_1` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`),
  CONSTRAINT `view_information_ibfk_2` FOREIGN KEY (`information_id`) REFERENCES `information` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `views`
--

DROP TABLE IF EXISTS `views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `width` smallint(5) unsigned NOT NULL,
  `height` smallint(5) unsigned NOT NULL,
  `locked` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `views_ibfk_1` (`organisation_id`),
  CONSTRAINT `views_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-01-03 16:15:21
