<?php
	define("IATLAS_VERSION", "1.2");

	# Index: C, I, A
	define("ASSET_VALUE", array(
		array(
			array(0, 0, 1),
			array(0, 0, 1),
			array(1, 1, 2)),
		array(
			array(0, 0, 1),
			array(0, 1, 2),
			array(1, 2, 2)),
		array(
			array(1, 1, 2),
			array(1, 2, 2),
			array(2, 2, 2)),
		array(
			array(1, 1, 2),
			array(1, 2, 2),
			array(2, 2, 2))));

	if (($_SESSION["language"] ?? null) == "nl") {
		define("CONFIDENTIALITY", array("openbaar", "intern", "vertrouwelijk", "geheim"));
		define("INTEGRITY", array("normaal", "belangrijk", "cruciaal"));
		define("AVAILABILITY", array("normaal", "belangrijk", "cruciaal"));
		define("ASSET_VALUE_LABELS", array("normaal", "belangrijk", "essentieel"));
		define("CONNECTION_DATA_FLOW", array("ophalen", "opsturen", "bidirectioneel"));
		define("FILE_DATA_FLOW", array("lezen", "schrijven", "lezen/schrijven"));
		define("LOCATION", array("intern", "extern", "SAAS Europees", "SAAS niet-Europees"));
		define("INFORMATION_STORAGE_TYPES", array("hoofdlocatie", "kopie"));
		define("PROCESSING_ROLES", array("verwerkingsverantwoordelijke", "verwerker"));

		define("LIST_TYPES", array(
			"security"  => "Applicatiebeveiligingsbehoefte",
			"files"     => "Bestandoperaties",
			"input"     => "Handmatige invoer van informatie",
			"internet"  => "Eigen aan internet gekoppelde applicaties",
			"protocols" => "Protocollen gebruikt in verbindingen"));
	} else {
		define("CONFIDENTIALITY", array("public", "internal", "confidential", "secret"));
		define("INTEGRITY", array("normal", "important", "crucial"));
		define("AVAILABILITY", array("normal", "important", "crucial"));
		define("ASSET_VALUE_LABELS", array("normal", "important", "essential"));
		define("CONNECTION_DATA_FLOW", array("download", "upload", "bidirectional"));
		define("FILE_DATA_FLOW", array("read", "write", "read/write"));
		define("LOCATION", array("internal", "external", "SAAS European", "SAAS non-European"));
		define("INFORMATION_STORAGE_TYPES", array("main location", "copy"));
		define("PROCESSING_ROLES", array("controller", "processor"));

		define("LIST_TYPES", array(
			"security"  => "Application security need",
			"files"     => "File operations",
			"input"     => "Manual input of information",
			"internet"  => "Own internet facing applications",
			"protocols" => "Protocols used in connections"));
	}
?>
