function role_change() {
	if ($('select#role option:selected').text() == 'controller') {
		$('div.controller').show();
		$('div.processor').hide();
	} else {
		$('div.controller').hide();
		$('div.processor').show();
	}
}

$(document).ready(function() {
	role_change();
});
